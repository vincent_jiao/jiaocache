package com.jiao.support.proxy;

import com.jiao.support.abs.CacheSupportAbstract;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @Author: vincent.jiao
 * @Date: 2021/4/17
 */
public class CacheProxy implements InvocationHandler {
    private CacheSupportAbstract cacheSupport;

    public CacheProxy(CacheSupportAbstract cacheSupport){
        this.cacheSupport = cacheSupport;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object value = null;

        try {
            value = method.invoke(cacheSupport, args);
        }catch (Exception e){
            throw e;
        }

        return value;
    }
}