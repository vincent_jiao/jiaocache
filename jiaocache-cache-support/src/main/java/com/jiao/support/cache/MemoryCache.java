package com.jiao.support.cache;

import org.apache.lucene.util.RamUsageEstimator;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.TimeUnit;

/**
 * 实现内存缓存.
 * <p>
 *     缓存超时:
 *          TODO 定时删除与懒删除
 *          在设置过期时间时, 计算出超时的秒值, 有序(时间从大到小的排列)的存入过期队列.
 *          定时任务每隔指定时间就扫描队队头, 直到扫到大于当前时间的
 * </p>
 * <p>
 *     过期策略: 使用 LRU 算法去清理最不常使用的数据.
 * </p>
 * @Author: vincent.jiao
 * @Date: 2021/6/27
 */
@Component
public class MemoryCache<K, V> {
//    @Autowired
//    CacheSysProperties properties;

    //缓存支持最大内容
    public static int maxMoney = 100;

    private final Map<K, V> data = new ConcurrentHashMap<>(2 << 8);

    /**
     * 记录逾期 key 与 时间.
     * 按时间的升序记录逾期key，同一时间可能存在多个逾期key
     */
    private final ConcurrentSkipListMap<Long, Set<K>> expireMap =
            new ConcurrentSkipListMap<>((Long o1, Long o2) -> {
                long k1 = o1.longValue();
                long k2 = o2.longValue();
                return k1 == k2 ? 0 : k1 >= k2 ? 1 : -1;
            });

    /** 逾期的定时任务 */
    private Timer clearExpireTimer = new Timer();
    private TimerTask clearExpireKeyTask = new TimerTask() {
        @Override
        public void run() {
            cleanExpireKey();
        }
    };

    /** 清理内存任务 */
    private final Timer clearMoneyTimer = new Timer();
    private TimerTask clearMoneyTask = new TimerTask() {
        @Override
        public void run() {
            evictKey();
        }
    };

    public MemoryCache() {
        //启动定时清理内存
        clearMoneyTimer.schedule(clearMoneyTask, 0, 1000 * 60);

        //启动定时清理逾期key
        clearExpireTimer.schedule(clearExpireKeyTask, 0, 1000 * 60);
    }

    /** 浏览历史(lur 队列),始终将活跃的放入头部,当查找数据时候会增加 O(n) 时间复杂度 */
    private LinkedList<K> browseHistory = new LinkedList<>();

    /**
     * 返回指定 k 对应的 value.
     * @param k
     * @return
     */
    public V get(K k){
        V v = data.get(k);
        if (v != null){
            pushStackBrowseHistorySafe(k);
        }

        return v;
    }

    /**
     * 添加值.
     * @param k
     * @param v
     * @return
     */
    public V put(K k, V v){
        V old = null;
        synchronized (k){
            old = data.put(k, v);
        }

        pushStackBrowseHistorySafe(k);
        return old;
    }

    /**
     * 添加值并设置逾期时间.
     * @param k
     * @param v
     * @param timeout
     * @param unit
     * @return
     */
    public boolean putExpire(K k, V v, long timeout, TimeUnit unit){
        synchronized (k){
            put(k, v);
            return expire(k, timeout, unit);
        }
    }

    /**
     * 逾期 key 设置.
     * @param k
     * @param timeout
     * @param unit
     * @return
     */
    public boolean expire(K k, long timeout, TimeUnit unit){
        long time = getExpectSecondTime(timeout, unit);

        //如果这个 key 之前存在逾期，那么就删除再设置
        for (Set<K> itemSet : expireMap.values()){
            synchronized (itemSet) {
                itemSet.remove(k);
            }
        }

        if (!contains(k)){
            return false;
        }

        //并发处理
        Set<K> expireSet = expireMap.get(k);
        if (expireSet == null){
            synchronized (expireMap){
                if ((expireSet = expireMap.get(k)) == null){
                    expireSet = new HashSet<>();
                    expireMap.put(time, expireSet);
                }
            }
        }

        boolean flag = false;
        synchronized (expireSet){
            flag = expireSet.add(k);
        }

        return flag;
    }

    /**
     * 获取数据总长度.
     * @return
     */
    public int size(){
        return data.size();
    }

    /**
     * 是否存在指定key.
     * @return
     */
    public boolean contains(K k){
        return data.containsKey(k);
    }

    /**
     * 通知逾期定时任务.
     * 从逾期表头开始获取时间，设置线程指定时间为用户设置的过期时间。而不是一直轮询
     */
//    private void noticeExpireScheduleTask(){
//        Map.Entry<Long, Queue<K>> expireEntry = expireMap.firstEntry();
//        if (expireEntry == null){
//            return;
//        }
//
////            if (isRunExpireTimer.get()){
//        //取消之前的任务。用于重新设置下一个任务的执行时间，因为可能此次设置的逾期时间是最小的
//        //取消并不是暴力取消，如果当前存在正在执行的不会停止。也就意味着可能会出现有多个线程在执行任务
//        clearExpireTimer.cancel();
////            }
//
//        //定时指定时间移除任务
//        clearExpireTimer.schedule(clearExpireKeyTask, new Date(expireEntry.getKey()));
////            isRunExpireTimer.set(true);
//    }

    /**
     * 根据指定 key 移除数据.
     * 如果key设置了预期时间，那么不会主动移除逾期 {@link #expireMap}
     * @param k
     * @return
     */
    public V remove(K k){
        V val = null;

        synchronized (k){
            val = data.remove(k);
        }

        if (val != null){
            synchronized (browseHistory){
                browseHistory.remove(k);
            }
        }

        return val;
    }

    private long getExpectSecondTime(long timeout, TimeUnit unit){
        long time = 0;
        switch (unit){
            case SECONDS:
                break;
            case MINUTES:
                time = timeout * 60;
                break;
            case HOURS:
                time = timeout * 60 * 60;
                break;
            case DAYS:
                time = timeout * 24 * 60 * 60;
                break;
            default:
                throw new IllegalArgumentException("不支持的时间单位");
        }

        time = time + LocalDateTime.now().getSecond();
        return time;
    }

    /**
     * 清理内存.
     */
    public void cleanMoney(){
        cleanExpireKey();
        evictKey();
    }

    /**
     * 驱逐策略.
     * 使用 lur 算法驱逐, 当达到指定内存使用量后开始驱逐.
     * 驱逐并不意味着立马清理内存, 而且让对象失去和 GC ROOT 的关联等待下次 GC 回收
     */
    private void evictKey(){
        //计算内存是否该清理
        int mb = new Double(Math.ceil(RamUsageEstimator.sizeOf(expireMap) / 1048576.0)).intValue();
        if (mb < maxMoney){
            return;
        }

        //清理缓存数据，默认清理百分之10,
        int dataSize = expireMap.size(), cleanSize = 0;
        float cleanCapacity = 0.1f;

        cleanSize = new Double(Math.ceil(dataSize * cleanCapacity)).intValue();
        if (cleanSize == 0){
            return;
        }

        for (int i = 0; i < cleanSize; i++) {
            K k = browseHistory.pollLast();
            if (k != null){
                data.remove(k);
            }
        }
    }

    /**
     * 清理过期 key.
     */
    private void cleanExpireKey(){
        long time = System.currentTimeMillis() / 1000;

        while (true){
            Map.Entry<Long, Set<K>> entry = expireMap.firstEntry();
            if (entry == null || entry.getKey() > time){
                break;
            }

            data.remove(entry.getValue());
            expireMap.pollFirstEntry();
        }
    }


    /**
     * 将浏览历史压栈.
     * @param k
     */
    private void pushStackBrowseHistorySafe(K k){
        synchronized (browseHistory){
            browseHistory.remove(k);
            browseHistory.push(k);
        }
    }
}
