package com.jiao.support.cache;

import com.jiao.support.abs.CacheSupportAbstract;
import com.jiao.support.proxy.CacheProxy;
import com.jiao.support.rule.CacheSupport;
import com.jiao.support.rule.ExecBatch;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * 动态缓存.
 * <p>
 *     实际是一个空实现，内部委托给 <code>currUseCache</code> 去实现
 * </p>
 * @Author: vincent.jiao
 * @Date: 2021/4/17
 */
@Component
public class DynamicCache extends CacheSupportAbstract {
    @Getter
    private CacheSupport remoteCache;

    @Getter
    private CacheSupport localCache;

    @Getter
    @Setter
    public CacheSupport currUseCache;

    public DynamicCache(@Qualifier("remoteCache") CacheSupportAbstract remoteCache,
                        @Qualifier("defaultLocalCache") CacheSupportAbstract localCache
                        ){

        CacheSupport remoteCacheObj = null;
        CacheSupport localCacheObj = null;

        if (remoteCache != null) {
            InvocationHandler remoteCacheProxy = new CacheProxy(remoteCache);
            remoteCacheObj = (CacheSupport) Proxy.newProxyInstance(
                    remoteCache.getClass().getClassLoader()
                    , new Class[]{CacheSupport.class}, remoteCacheProxy);
        }

        if (localCache != null) {
            InvocationHandler localCacheProxy = new CacheProxy(localCache);
            localCacheObj = (CacheSupport) Proxy.newProxyInstance(
                    localCache.getClass().getClassLoader()
                    , new Class[]{CacheSupport.class}, localCacheProxy);
        }

        this.localCache = localCacheObj;
        this.remoteCache = remoteCacheObj;

        currUseCache = this.remoteCache;
    }

    /**
     * 是否为远程缓存.
     * @return
     */
    public boolean isRemoteCache(){
        return currUseCache == remoteCache;
    }

    @Override
    public int listSize(String key) {
        return currUseCache.listSize(key);
    }

    @Override
    public boolean listContains(String key, Object o) {
        return currUseCache.listContains(key, o);
    }

    @Override
    public <E> boolean listAdd(String key, E e) {
        return currUseCache.listAdd(key, e);
    }

    @Override
    public <E> boolean listaddAll(String key, Collection<? extends E> c) {
        return currUseCache.listaddAll(key, c);
    }

    @Override
    public <E> E listGet(String key, int index) {
        return currUseCache.listGet(key, index);
    }

    @Override
    public <E> void listSetByIndex(String key, int index, E element) {
        currUseCache.listSetByIndex(key, index, element);
    }

    @Override
    public <E> E listRemove(String key, int index) {
        return currUseCache.listRemove(key, index);
    }

    @Override
    public <E> Long setAdd(String key, E[] values) {
        return  currUseCache.setAdd(key, values);
    }

    @Override
    public Long setRemove(String key, Object... values) {
        return currUseCache.setRemove(key, values);
    }

    @Override
    public <E> E setPop(String key) {
        return currUseCache.setPop(key);
    }

    @Override
    public <E> List<E> setPop(String key, long count) {
        return currUseCache.setPop(key, count);
    }

    @Override
    public <E> Boolean setMove(String key, E value, String destKey) {
        return currUseCache.setMove(key, value, destKey);
    }

    @Override
    public Long size(String key) {
        return currUseCache.setPop(key);
    }

    @Override
    public boolean setContains(String key, Object o) {
        return currUseCache.setContains(key, o);
    }

    @Override
    public <E> Set<E> setIntersect(String key, String otherKey) {
        return currUseCache.setIntersect(key, otherKey);
    }

    @Override
    public <E> Set<E> setIntersect(String key, Collection<String> otherKeys) {
        return currUseCache.setIntersect(key, otherKeys);
    }

    @Override
    public <E> Set<E> setIntersect(Collection<String> keys) {
        return currUseCache.setIntersect(keys);
    }

    @Override
    public <E> Set<E> setUnion(String key, String otherKey) {
        return currUseCache.setUnion(key, otherKey);
    }

    @Override
    public <E> Set<E> setUnion(String key, Collection<String> otherKeys) {
        return currUseCache.setUnion(key, otherKeys);
    }

    @Override
    public <E> Set<E> setUnion(Collection<String> keys) {
        return currUseCache.setUnion(keys);
    }

    @Override
    public <E> Set<E> setDifference(String key, String otherKey) {
        return currUseCache.setDifference(key, otherKey);
    }

    @Override
    public <E> Set<E> setDifference(String key, Collection<String> otherKeys) {
        return currUseCache.setDifference(key, otherKeys);
    }

    @Override
    public <E> Set<E> setDifference(Collection<String> keys) {
        return currUseCache.setDifference(keys);
    }

    @Override
    public <E> Set<E> setgetAll(String key) {
        return currUseCache.setgetAll(key);
    }

    @Override
    public <E> E setRandomMember(String key) {
        return currUseCache.setRandomMember(key);
    }

    @Override
    public <E> Set<E> setDistinctRandomMembers(String key, long count) {
        return currUseCache.setDistinctRandomMembers(key, count);
    }

    @Override
    public <E> List<E> setRandomGet(String key, long count) {
        return currUseCache.setRandomGet(key, count);
    }

    @Override
    public <E> List<E> queueRange(String key, long start, long end) {
        return currUseCache.queueRange(key, start, end);
    }

    @Override
    public Long queueSize(String key) {
        return currUseCache.queueSize(key);
    }

    @Override
    public <E> Long queueLeftPush(String key, E value) {
        return currUseCache.queueLeftPush(key, value);
    }

    @Override
    public <E> Long queueLeftPushAll(String key, E[] values) {
        return currUseCache.queueLeftPushAll(key, values);
    }

    @Override
    public <E> Long queueLeftPushAll(String key, Collection<E> var2) {
        return currUseCache.queueLeftPushAll(key, var2);
    }

    @Override
    public <E> Long queueLeftPushIfPresent(String key, E var2) {
        return currUseCache.queueLeftPushIfPresent(key, var2);
    }

    @Override
    public <E> Long queueRightPush(String key, E var2) {
        return currUseCache.queueRightPush(key, var2);
    }

    @Override
    public <E> Long queueRightPushAll(String key, E[] e) {
        return currUseCache.queueRightPushAll(key, e);
    }

    @Override
    public <E> Long queueRightPushAll(String key, Collection<E> e) {
        return currUseCache.queueRightPushAll(key, e);
    }

    @Override
    public <E> Long queueRightPushIfPresent(String key, E e) {
        return currUseCache.queueRightPushIfPresent(key, e);
    }

    @Override
    public <E> void queueSet(String key, long index, E e) {
        currUseCache.queueSet(key, index, e);
    }

    @Override
    public Long queueRemove(String key, long count, Object value) {
        return currUseCache.queueRemove(key, count, value);
    }

    @Override
    public <E> E queueIndex(String key, long index) {
        return currUseCache.queueIndex(key, index);
    }

    @Override
    public <E> E queueLeftPop(String key) {
        return currUseCache.queueLeftPop(key);
    }

    @Override
    public <E> E queueRightPop(String key) {
        return currUseCache.queueRightPop(key);
    }

    @Override
    public Long mapDelete(String key, String... hashKeys) {
        return currUseCache.mapDelete(key, hashKeys);
    }

    @Override
    public Boolean mapHasKey(String key, String hashKey) {
        return currUseCache.mapHasKey(key, hashKey);
    }

    @Override
    public <K, V> V mapGet(String key, String hashKey) {
        return currUseCache.mapGet(key, hashKey);
    }

    @Override
    public <K, V> List<V> mapMultiGet(String key, Collection<String> hashKeys) {
        return currUseCache.mapMultiGet(key, hashKeys);
    }

    @Override
    public <K, V> Set<K> mapKeys(String key) {
        return currUseCache.mapKeys(key);
    }

    @Override
    public <K, V> Long mapLengthOfValue(String key, String hashKey) {
        return currUseCache.mapLengthOfValue(key, hashKey);
    }

    @Override
    public Long mapSize(String key) {
        return currUseCache.mapSize(key);
    }

    @Override
    public <K, V> void mapPutAll(String key, Map<? extends K, ? extends V> m) {
        currUseCache.mapPutAll(key, m);
    }

    @Override
    public <K, V> void mapPut(String key, String hashKey, V value) {
        currUseCache.mapPut(key, hashKey, value);
    }

    @Override
    public <K, V> Boolean mapPutIfAbsent(String key, String hashKey, V value) {
        return currUseCache.mapPutIfAbsent(key, hashKey, value);
    }

    @Override
    public <K, V> List<V> mapValues(String key) {
        return currUseCache.mapValues(key);
    }

    @Override
    public <K, V> Map<K, V> mapEntries(String key) {
        return currUseCache.mapEntries(key);
    }

    @Override
    public Boolean expire(String key, long timeout, TimeUnit unit) {
        return currUseCache.expire(key, timeout, unit);
    }

    @Override
    public Boolean delete(String key) {
        return currUseCache.delete(key);
    }

    @Override
    public Long delete(Collection<String> keys) {
        return currUseCache.delete(keys);
    }

    @Override
    public boolean contains(String key) {
        return currUseCache.delete(key);
    }

    /**
     * 用于命令的扩展.
     * @param execBatch
     * @param <T>
     * @return
     */
    @Override
    public <T> List<T> extendCommand(ExecBatch<T> execBatch) {
        return currUseCache.extendCommand(execBatch);
    }


    @Override
    public void bitSetBit(String key, long offset, boolean flag) {
        currUseCache.bitSetBit(key, offset, flag);
    }

    @Override
    public boolean bitGetBit(String key, long offset) {
        return currUseCache.bitGetBit(key, offset);
    }

    @Override
    public Collection<Long> bitGetExistsBit(String key, long min, long max) {
        return currUseCache.bitGetExistsBit(key, min, max);
    }

    @Override
    public Collection<Long> bitGetExistsBit(String key, Collection<Long> offsets) {
        return currUseCache.bitGetExistsBit(key, offsets);
    }

    @Override
    public Long bitGetCount(String key) {
        return currUseCache.bitGetCount(key);
    }

    @Override
    public void valueSet(String key, Object value) {
        currUseCache.valueSet(key, value);
    }

    @Override
    public void valueSet(String key, Object value, long timeout, TimeUnit unit) {
        currUseCache.valueSet(key, value, timeout, unit);
    }

    @Override
    public Boolean valueSetIfAbsent(String key, Object value) {
        return currUseCache.valueSetIfAbsent(key, value);
    }

    @Override
    public Boolean valueSetIfAbsent(String key, Object value, long timeout, TimeUnit unit) {
        return currUseCache.valueSetIfAbsent(key, value, timeout, unit);
    }

    @Override
    public Boolean valueSetIfPresent(String key, Object value) {
        return currUseCache.valueSetIfPresent(key, value);
    }

    @Override
    public Boolean valueSetIfPresent(String key, Object value, long timeout, TimeUnit unit) {
        return currUseCache.valueSetIfPresent(key, value, timeout, unit);
    }

    @Override
    public void valueMultiSet(Map<String, Object> map) {
        currUseCache.valueMultiSet(map);
    }

    @Override
    public Boolean valueMultiSetIfAbsent(Map<String, Object> map) {
        return currUseCache.valueMultiSetIfAbsent(map);
    }

    @Override
    public <T> T valueGet(String key) {
        return currUseCache.valueGet(key);
    }

    @Override
    public <T> T getAndSet(String key, T value) {
        return currUseCache.getAndSet(key, value);
    }

    @Override
    public <V> List<V> multiGet(Collection<String> keys) {
        return currUseCache.multiGet(keys);
    }

    @Override
    public Long increment(String key) {
        return currUseCache.increment(key);
    }

    @Override
    public Long increment(String key, long delta) {
        return currUseCache.increment(key, delta);
    }

    @Override
    public Long decrement(String key) {
        return currUseCache.decrement(key);
    }

    @Override
    public Long decrement(String key, long delta) {
        return currUseCache.decrement(key, delta);
    }

    @Override
    public Integer append(String key, String value) {
        return currUseCache.append(key, value);
    }
}
