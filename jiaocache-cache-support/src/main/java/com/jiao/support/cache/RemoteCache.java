package com.jiao.support.cache;

import cn.hutool.core.collection.CollectionUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.jiao.comm.utils.TypeConvert;
import com.jiao.support.abs.CacheSupportAbstract;
import com.jiao.support.rule.ExecBatch;
import com.jiao.support.script.LuaScriptText;
import lombok.SneakyThrows;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.BitFieldSubCommands;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.ReturnType;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 默认远程缓存.
 * redis 只有链表的实现，没有基于数组的数据结构，
 * 为了和本地缓存的数据结构做匹配，所以这里默认都调用队列的 api。
 * 因为数组和链表在查找上的时间复杂度分别为: O(1)、O(n) 查找性能上差距是比较大的，
 * 所以这里做了一下支撑
 * @Author: vincent.jiao
 * @Date: 2021/4/18
 */
@Component
//@ConditionalOnBean( name = "redisTemplate" )
public class RemoteCache extends CacheSupportAbstract {
    public static ObjectMapper objectMapper = new ObjectMapper();

    static {
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS,false);
        objectMapper.setFilterProvider(new SimpleFilterProvider().setFailOnUnknownId(false));
        //就算是 null 也序列化
        objectMapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);
    }

    RedisTemplate redisTemplate;
    SetOperations setOperations = null;
    HashOperations hashOperations = null;
    ListOperations listOperations = null;
    ValueOperations valueOperations = null;

    private String bitRangeScriptSha;
    private byte[] bitRangeScriptByteSha;

    public RemoteCache( RedisTemplate redisTemplate){
        this.redisTemplate = redisTemplate;
        setOperations = redisTemplate.opsForSet();
        hashOperations = redisTemplate.opsForHash();
        listOperations = redisTemplate.opsForList();
        valueOperations = redisTemplate.opsForValue();

        String scriptText = LuaScriptText.getBitRangeScriptText();
        List result = redisTemplate.executePipelined(new RedisCallback<Object>(){
            @Override
            public Object doInRedis(RedisConnection connection) throws DataAccessException {
                connection.scriptLoad(scriptText.getBytes());
                return null;
            }
        }, redisTemplate.getValueSerializer());

        bitRangeScriptSha = result.get(0).toString();
        bitRangeScriptByteSha = bitRangeScriptSha.getBytes();
    }

    @Override
    public int listSize(String key){
        Long size = queueSize(key);
        return size == null ? null : size.intValue();
    }

    @Override
    public boolean listContains(String key, Object o) {
        return queueIndex(key, 0) != null;
    }

    @Override
    public <E> boolean listAdd(String key, E e) {
        queueRightPush(key, e);
        return true;
    }

    @Override
    public <E>  boolean listaddAll(String key, Collection<? extends E> c) {
        queueRightPushAll(key, c);
        return true;
    }

    @Override
    public  <E> E listGet(String key, int index) {
        return queueIndex(key, index);
    }

    @Override
    public <E>  void listSetByIndex(String key, int index, E element) {
        queueSet(key, index, element);
    }

    @Override
    public <E> E listRemove(String key, int index){
        E o = listGet(key, index);
        if(o != null){
            queueRemove(key, 1, o);
        }

        return o;
    }

    @Override
    public <E> List<E> queueRange(String key, long start, long end){
        return listOperations.range(key, start, end);
    }

    @Override
    public Long queueSize(String key) {
        return listOperations.size(key);
    }

    @Override
    public <E> Long queueLeftPush(String key, E value) {
        return listOperations.leftPush(key, value);
    }

    @Override
    public <E> Long queueLeftPushAll(String key, E... values) {
        return listOperations.leftPush(key, values);
    }

    @Override
    public <E> Long queueLeftPushAll(String key, Collection<E> var2) {
        return listOperations.leftPush(key, var2);
    }

    @Override
    public <E> Long queueLeftPushIfPresent(String key, E var2){
        return listOperations.leftPushIfPresent(key, var2);
    }

    @Override
    public <E> Long queueRightPush(String key, E var2) {
        return listOperations.rightPush(key, var2);
    }

    @Override
    public <E> Long queueRightPushAll(String key, E... e) {
        return listOperations.rightPush(key, e);
    }

    @Override
    public <E> Long queueRightPushAll(String key, Collection<E> e) {
        return listOperations.rightPushAll(key, e);
    }

    @Override
    public <E> Long queueRightPushIfPresent(String key, E e) {
        return listOperations.rightPushIfPresent(key, e);
    }

    @Override
    public  <E> void queueSet(String key, long index, E e) {
        listOperations.set(key, index, e);
    }

    @Override
    public Long queueRemove(String key, long count, Object value) {
        return listOperations.remove(key, count, value);
    }

    @Override
    public <E> E queueIndex(String key, long index) {
        return (E) listOperations.index(key, index);
    }

    @Override
    public <E> E queueLeftPop(String key) {
        ListOperations<String, E> operations = redisTemplate.opsForList();
        return operations.leftPop(key);
    }

    @Override
    public <E> E queueRightPop(String key) {
        ListOperations<String, E> operations = redisTemplate.opsForList();
        return operations.rightPop(key);
    }

    @Override
    public Long setAdd(String key, Object[] values) {
        return setOperations.add(key, values);
    }

    @Override
    public Long setRemove(String key, Object... values) {
        return setOperations.remove(key, values);
    }

    @Override
    public Object setPop(String key) {
        return setOperations.pop(key);
    }

    @Override
    public List setPop(String key, long count) {
        return setOperations.pop(key, count);
    }

    @Override
    public Boolean setMove(String key, Object value, String destKey) {
        return setOperations.move(key, value, destKey);
    }

    @Override
    public Long size(String key) {
        return setOperations.size(key);
    }

    @Override
    public boolean setContains(String key, Object o) {
        throw new UnsupportedOperationException("不支持此方法");
    }

    @Override
    public Set setIntersect(String key, String otherKey) {
        return setOperations.intersect(key, otherKey);
    }

    @Override
    public Set setIntersect(String key, Collection otherKeys) {
        return setOperations.intersect(key, otherKeys);
    }

    @Override
    public Set setIntersect(Collection keys) {
        return setOperations.intersect(keys);
    }

    @Override
    public Set setUnion(String key, String otherKey) {
        return setOperations.union(key, otherKey);
    }

    @Override
    public Set setUnion(String key, Collection otherKeys) {
        return setOperations.union(key, otherKeys);
    }

    @Override
    public <T> Set<T> setUnion(Collection<String> keys) {
        return setOperations.union(keys);
    }

    @Override
    public <E> Set<E> setDifference(String key, String otherKey) {
        return setOperations.difference(key, otherKey);
    }

    @Override
    public <E> Set<E> setDifference(String key, Collection<String> otherKeys) {
        return setOperations.difference(key, otherKeys);
    }

    @Override
    public <E> Set<E> setDifference(Collection<String> keys) {
        return setOperations.difference(keys);
    }

    @Override
    public <E> Set<E> setgetAll(String key) {
        return setOperations.members(key);
    }

    @Override
    public  <E> E setRandomMember(String key) {
        SetOperations<String, E> operations = redisTemplate.opsForSet();
        return operations.randomMember(key);
    }

    @Override
    public <E> Set<E> setDistinctRandomMembers(String key, long count) {
        return setOperations.distinctRandomMembers(key, count);
    }

    @Override
    public  <E> List<E> setRandomGet(String key, long count){
        return setOperations.randomMembers(key, count);
    }

    @Override
    public Long mapDelete(String key, String... hashKeys) {
        return hashOperations.delete(key, hashKeys);
    }

    @Override
    public Boolean mapHasKey(String key, String hashKey) {
        return hashOperations.hasKey(key, hashKey);
    }

    @Override
    public <K, V> V mapGet(String key, String hashKey) {
        HashOperations<String, K, V> operations = redisTemplate.opsForHash();
        return operations.get(key, hashKey);
    }

    @Override
    public <K, V> List<V>  mapMultiGet(String key, Collection<String> hashKeys) {
        return hashOperations.multiGet(key, hashKeys);
    }

    @Override
    public <K, V> Set<K>  mapKeys(String key) {
        return hashOperations.keys(key);
    }

    @Override
    public Long mapLengthOfValue(String key, String hashKey) {
        return hashOperations.lengthOfValue(key, hashKey);
    }

    @Override
    public Long mapSize(String key) {
        return hashOperations.size(key);
    }

    @Override
    public <K, V>  void mapPutAll(String key, Map<? extends K, ? extends V> m) {
        hashOperations.putAll(key, m);
    }

    @Override
    public <K, V>  void mapPut(String key, String hashKey, V value) {
        hashOperations.put(key, hashKey, value);
    }

    @Override
    public Boolean mapPutIfAbsent(String key, String hashKey, Object value) {
        return hashOperations.putIfAbsent(key, hashKey, value);
    }

    @Override
    public List mapValues(String key) {
        return hashOperations.values(key);
    }

    @Override
    public Map mapEntries(String key) {
        return hashOperations.entries(key);
    }

    @Override
    public Boolean expire(String key, long timeout, TimeUnit unit) {
        return redisTemplate.expire(key, timeout, unit);
    }

    @Override
    public Boolean delete(String key) {
        return redisTemplate.delete(key);
    }

    @Override
    public Long delete(Collection<String> keys) {
        return redisTemplate.delete(keys);
    }

    @Override
    public boolean contains(String key) {
        return redisTemplate.hasKey(key);
    }

    @Override
    public void bitSetBit(String key, long offset, boolean flag) {
        redisTemplate.opsForValue().setBit(key, offset, flag);
    }

    @Override
    public boolean bitGetBit(String key, long offset) {
        return redisTemplate.opsForValue().getBit(key, offset);
    }

    @Override
    public Collection<Long> bitGetExistsBit(String key, long min, long max) {
         List<Long> datas = (List<Long>) redisTemplate.execute(new RedisCallback() {
            @Override
            public Object doInRedis(RedisConnection connection) throws DataAccessException {
                Object obj = null;
                try {
                    obj = connection.scriptingCommands().evalSha(bitRangeScriptByteSha,
                            ReturnType.MULTI, 1,
                            objectMapper.writeValueAsString(Arrays.asList(key, min, max)).getBytes());
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }

                return obj;
            }
        });
         return datas;
    }

    @Override
    public Collection<Long> bitGetExistsBit(String key, Collection<Long> offsets) {
        //TODO 目前是放入管道中实现，但是建议使用 lua 实现
        List objList = redisTemplate.executePipelined(new RedisCallback<List>() {
            @Override
            public List doInRedis(RedisConnection connection) throws DataAccessException {
                for (Long item : offsets) {
                    connection.getBit(keySerializer(key), item);
                }
                return null;
            }
        });
        
        List<Long> existsList = new LinkedList<>();
        int i = -1;
        for (Long item : offsets) {
            i++;
            if(Boolean.valueOf(objList.get(i).toString())) {
                existsList.add(item);
            }
        }

        return existsList;
    }

    @Override
    public Long bitGetCount(String key) {
        return (Long) redisTemplate.execute(new RedisCallback<Long>() {
            @Override
            public Long doInRedis(RedisConnection redisConnection) throws DataAccessException {
                return redisConnection.bitCount(keySerializer(key));
            }
        });
    }

    @Override
    public void valueSet(String key, Object value) {
        valueOperations.set(key, value);
    }

    @Override
    public void valueSet(String key, Object value, long timeout, TimeUnit unit) {
        valueOperations.set(key, value, timeout, unit);
    }

    @Override
    public Boolean valueSetIfAbsent(String key, Object value) {
        return valueOperations.setIfAbsent(key, value);
    }

    @Override
    public Boolean valueSetIfAbsent(String key, Object value, long timeout, TimeUnit unit) {
        return valueOperations.setIfAbsent(key, value, timeout, unit);
    }

    @Override
    public Boolean valueSetIfPresent(String key, Object value) {
        return valueOperations.setIfPresent(key, value);
    }

    @Override
    public Boolean valueSetIfPresent(String key, Object value, long timeout, TimeUnit unit) {
        return valueOperations.setIfPresent(key, value, timeout, unit);
    }

    @Override
    public void valueMultiSet(Map<String, Object> map) {
        valueOperations.multiSet(map);
    }

    @Override
    public Boolean valueMultiSetIfAbsent(Map<String, Object> map) {
        return valueOperations.multiSetIfAbsent(map);
    }

    @Override
    public <T> T valueGet(String key) {
        return (T) redisTemplate.opsForValue().get(key);
    }

    @Override
    public <T> T getAndSet(String key, T value) {
        return (T) valueOperations.getAndSet(key, value);
    }

    @Override
    public <V> List<V> multiGet(Collection<String> keys) {
        return valueOperations.multiGet(keys);
    }

    @Override
    public Long increment(String key) {
        return valueOperations.increment(key);
    }

    @Override
    public Long increment(String key, long delta) {
        return valueOperations.increment(key, delta);
    }

    @Override
    public Long decrement(String key) {
        return valueOperations.decrement(key);
    }

    @Override
    public Long decrement(String key, long delta) {
        return valueOperations.decrement(key, delta);
    }

    @Override
    public Integer append(String key, String value) {
        return valueOperations.append(key, value);
    }

    /**
     * 使用通道批量执行.
     * 在通道内所有方法的返回会一直返回 null, 并且 ExecBatch 体内方法也必须返回 null.
     * 而结果在通道执行完毕后获取, 以 list 形式获取
     * @param execBatch
     * @param <T>
     * @return
     */
    @Override
    public <T> List<T> extendCommand(ExecBatch<T> execBatch) {
        List<T> result = redisTemplate.executePipelined(new RedisCallback<T>(){
            @Override
            public T doInRedis(RedisConnection connection) throws DataAccessException {
                execBatch.exec(connection, redisTemplate);
                return null;
            }
        }, redisTemplate.getValueSerializer());

        if(result == null){
            return Collections.emptyList();
        }

        return result;
    }

    public byte[] keySerializer(String key) {
        return redisTemplate.getKeySerializer().serialize(key);
    }
}
