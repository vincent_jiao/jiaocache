package com.jiao.support.cache;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.jiao.support.abs.CacheSupportAbstract;
import com.jiao.support.rule.ExecBatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/** 本地缓存.
 * 对比过 Caffeine 与 guava 这 2 个缓存框架，但是发现他们对这个 api 支持非常不好，
 * 所以这里实现个自己的缓存系统.
 * @Author: vincent.jiao
 * @Date: 2021/5/14
 */
@Component
public class DefaultLocalCache extends CacheSupportAbstract {
    @Autowired
    MemoryCache memoryCache;
    private Random random = new Random();

    /**
     * 返回 list 集合.
     * 如果不存在就创建
     *
     * @param key
     * @param <T>
     * @return
     */
    private <T> List<T> getList(String key){
        List<T> list = null;
        synchronized (key){
            Object value = memoryCache.get(key);
            if (value == null){
                memoryCache.put(key, list = new ArrayList<>());
            } else {
                list = (List<T>) value;
            }
        }

        return list;
    }

    @Override
    public int listSize(String key) {
        return getList(key).size();
    }

    @Override
    public boolean listContains(String key, Object o) {
        synchronized (key) {
            return getList(key).contains(o);
        }
    }

    @Override
    public <E> boolean listAdd(String key, E e) {
        synchronized (key){
            return getList(key).add(e);
        }
    }

    @Override
    public <E> boolean listaddAll(String key, Collection<? extends E> c) {
        synchronized (key){
            return getList(key).addAll(c);
        }
    }

    @Override
    public <E> E listGet(String key, int index) {
        synchronized (key){
            return (E) getList(key).get(index);
        }
    }

    @Override
    public <E> void listSetByIndex(String key, int index, E element) {
        synchronized (key){
            getList(key).set(index, element);
        }
    }

    @Override
    public <E> E listRemove(String key, int index) {
        synchronized (key){
            return (E) getList(key).remove(index);
        }
    }

    private BitSet getBitSet(String key){
        BitSet bitSet = null;
        synchronized (key){
            Object value = memoryCache.get(key);

            if (value == null){
                memoryCache.put(key, bitSet = new BitSet());
            } else {
                bitSet = (BitSet) value;
            }
        }

        return bitSet;
    }

    @Override
    public void bitSetBit(String key, long offset, boolean flag) {
        getBitSet(key).set(new Long(offset).intValue(), flag);
    }

    @Override
    public boolean bitGetBit(String key, long offset) {
        return getBitSet(key).get(new Long(offset).intValue());
    }

    @Override
    public Collection<Long> bitGetExistsBit(String key, long min, long max) {
        BitSet bitSet = getBitSet(key);
        List<Long> values = new LinkedList<>();

        for (long i = min; i <= max ; i++) {
            if (bitSet.get(new Long(i).intValue())){
                values.add(i);
            }
        }

        return values;
    }

    @Override
    public Collection<Long> bitGetExistsBit(String key, Collection<Long> offsets) {
        BitSet bitSet = getBitSet(key);
        List<Long> values = new LinkedList<>();

        for (Long i : offsets) {
            if (bitSet.get(i.intValue())){
                values.add(i);
            }
        }

        return values;
    }

    @Override
    public Long bitGetCount(String key) {
        return new Long(getBitSet(key).cardinality());
    }

    public ConcurrentSkipListSet getSet(String key){
        synchronized (key){
//            Set set =  memoryCache.get(key);
        }
        return null;
    }

    @Override
    public <E> Long setAdd(String key, E[] values) {
        return null;
    }

    @Override
    public Long setRemove(String key, Object... values) {
        return null;
    }

    @Override
    public <E> E setPop(String key) {
        return (E) getSet(key).pollFirst();
    }

    @Override
    public <E> List<E> setPop(String key, long count) {
        ConcurrentSkipListSet set = getSet(key);
        List<E> list = new LinkedList<>();

        for (int i = 0; i < count; i++) {
            list.add((E) (i % 2 == 0 ? set.pollFirst() : set.pollLast()));
        }
        return list;
    }

    @Override
    public <E> Boolean setMove(String key, E value, String destKey) {
        ConcurrentSkipListSet sourceSet = getSet(key);
        if (sourceSet.remove(value)){
            return getSet(destKey).add(value);
        }

        return false;
    }

    @Override
    public Long size(String key) {
        return new Long(getSet(key).size());
    }

    @Override
    public boolean setContains(String key, Object o) {
        return getSet(key).contains(o);
    }

    @Override
    public <E> Set<E> setIntersect(String key, String otherKey) {
        ConcurrentSkipListSet sourceSet = getSet(key).clone();
        sourceSet.retainAll(getSet(otherKey));

        return sourceSet;
    }

    @Override
    public <E> Set<E> setIntersect(String key, Collection<String> otherKeys) {
        if (CollectionUtil.isEmpty(otherKeys)){
            return Collections.emptySet();
        }

        ConcurrentSkipListSet sourceSet = getSet(key).clone();
        for (String item : otherKeys){
            sourceSet.removeAll(getSet(item));
        }

        return sourceSet;
    }

    @Override
    public <E> Set<E> setIntersect(Collection<String> keys) {
        if (CollectionUtil.isEmpty(keys)){
            return Collections.emptySet();
        }

        ConcurrentSkipListSet sourceSet = null;
        for (String item : keys) {
            if (sourceSet == null){
                sourceSet = getSet(item).clone();
                continue;
            }

            sourceSet.retainAll(getSet(item));
        }

        return sourceSet;
    }

    @Override
    public <E> Set<E> setUnion(String key, String otherKey) {
        ConcurrentSkipListSet sourceSet = new ConcurrentSkipListSet();
        sourceSet.addAll(getSet(key));
        sourceSet.addAll(getSet(otherKey));

        return sourceSet;
    }

    @Override
    public <E> Set<E> setUnion(String key, Collection<String> otherKeys) {
        ConcurrentSkipListSet sourceSet = getSet(key).clone();
        if (CollectionUtil.isEmpty(otherKeys)){
            return sourceSet;
        }

        for (String item : otherKeys){
            sourceSet.addAll(getSet(item));
        }

        return sourceSet;
    }

    @Override
    public <E> Set<E> setUnion(Collection<String> keys) {
        if (CollectionUtil.isEmpty(keys)){
            return Collections.emptySet();
        }

        ConcurrentSkipListSet sourceSet = new ConcurrentSkipListSet();
        for (String item : keys){
            sourceSet.addAll(getSet(item));
        }

        return sourceSet;
    }

    @Override
    public <E> Set<E> setDifference(String key, String otherKey) {
        ConcurrentSkipListSet sourceSet = getSet(key).clone();
        sourceSet.removeAll(getSet(otherKey));

        return sourceSet;
    }

    @Override
    public <E> Set<E> setDifference(String key, Collection<String> otherKeys) {
        ConcurrentSkipListSet sourceSet = getSet(key).clone();
        if (CollUtil.isEmpty(otherKeys)){
            return sourceSet;
        }

        for (String item : otherKeys){
            sourceSet.removeAll(getSet(item));
        }

        return sourceSet;
    }

    @Override
    public <E> Set<E> setDifference(Collection<String> keys) {
        if (CollUtil.isEmpty(keys)){
            return Collections.emptySet();
        }

        ConcurrentSkipListSet sourceSet = null;
        for (String item : keys) {
            if (sourceSet == null){
                sourceSet = getSet(item).clone();
                continue;
            }

            sourceSet.removeAll(getSet(item));
        }

        return sourceSet;
    }

    @Override
    public <E> Set<E> setgetAll(String key) {
        return getSet(key).clone();
    }

    @Override
    public <E> E setRandomMember(String key) {
        return (E) getSet(key).first();
    }

    @Override
    public <E> Set<E> setDistinctRandomMembers(String key, long count) {
        Object[] objArr = getSet(key).toArray();
        Set<E> resultSet = new HashSet<>();

        for (int i = 0; i < count; i++) {
            if (!resultSet.add((E) objArr[random.nextInt(objArr.length)])){
                i--;
            }
        }

        return resultSet;
    }

    @Override
    public <E> List<E> setRandomGet(String key, long count) {
        Object[] objArr = getSet(key).toArray();
        List<E> resultSet = new LinkedList<>();

        for (int i = 0; i < count; i++) {
            resultSet.add((E) objArr[random.nextInt(objArr.length)]);;
        }

        return resultSet;
    }

    private LinkedList getQueue(String key){
        LinkedList  list = null;
        synchronized (key){
            if ((list = (LinkedList) memoryCache.get(key)) == null){
                memoryCache.put(key, list);
            }
        }

        return list;
    }

    @Override
    public <E> List<E> queueRange(String key, long start, long end) {
        List<E> resultList = new LinkedList<>();
        LinkedList<E> copyList = null;

        synchronized (key){
            copyList = (LinkedList<E>) getQueue(key).clone();
        }

        Iterator<E> iterator = copyList.iterator();

        for (int i = 0; iterator.hasNext(); i++) {
            if (i >= start && i <= end){
                resultList.add(iterator.next());
            }
        }

        return resultList;
    }

    @Override
    public Long queueSize(String key) {
        return new Long(getQueue(key).size());
    }

    @Override
    public <E> Long queueLeftPush(String key, E value) {
        synchronized (key){
            getQueue(key).addFirst(value);
        }
        return queueSize(key);
    }

    @Override
    public <E> Long queueLeftPushAll(String key, E[] values) {
        LinkedList<E> queue = getQueue(key);
        for (E item : values) {
            synchronized (key){
                queue.addFirst(item);
            }
        }
        
        return queueSize(key);
    }

    @Override
    public <E> Long queueLeftPushAll(String key, Collection<E> var2) {
        LinkedList<E> queue = getQueue(key);
        for (E item : var2) {
            synchronized (key){
                queue.addFirst(item);
            }
        }

        return queueSize(key);
    }

    @Override
    public <E> Long queueLeftPushIfPresent(String key, E var2) {
        LinkedList<E> queue = (LinkedList<E>) memoryCache.get(key);

        if (queue != null){
            synchronized (key){
                queue.addFirst(var2);
            }
        }

        return queueSize(key);
    }

    @Override
    public <E> Long queueRightPush(String key, E var2) {
        synchronized (key){
            getQueue(key).add(var2);
        }
        return queueSize(key);
    }

    @Override
    public <E> Long queueRightPushAll(String key, E[] e) {
        LinkedList<E> queue = (LinkedList<E>) memoryCache.get(key);
        for (E item : e) {
            synchronized (key){
                queue.add(item);
            }
        }

        return queueSize(key);
    }

    @Override
    public <E> Long queueRightPushAll(String key, Collection<E> e) {
        synchronized (key){
            getQueue(key).addAll(e);
        }
        return queueSize(key);
    }

    @Override
    public <E> Long queueRightPushIfPresent(String key, E e) {
        LinkedList<E> queue = (LinkedList<E>) memoryCache.get(key);

        if (queue != null){
            synchronized (key){
                queue.add(e);
            }
        }

        return queueSize(key);
    }

    @Override
    public <E> void queueSet(String key, long index, E e) {
        synchronized (key){
            getQueue(key).set(Long.valueOf(index).intValue(), e);
        }
    }

    @Override
    public Long queueRemove(String key, long count, Object value) {
        LinkedList queue = (LinkedList) memoryCache.get(key);

        synchronized (key){
            for (int j = 0; j < count; j++) {
                queue.removeFirst();
            }
        }

        return queueSize(key);
    }

    @Override
    public <E> E queueIndex(String key, long index) {
        LinkedList<E> queue = (LinkedList) memoryCache.get(key);

        synchronized (key){
            return queue.get(Long.valueOf(index).intValue());
        }
    }

    @Override
    public <E> E queueLeftPop(String key) {
        LinkedList<E> queue = (LinkedList) memoryCache.get(key);
        synchronized (key){
            return queue.removeFirst();
        }
    }

    @Override
    public <E> E queueRightPop(String key) {
        LinkedList<E> queue = (LinkedList) memoryCache.get(key);
        synchronized (key){
            return queue.removeLast();
        }
    }

    @Override
    public Long mapDelete(String key, String... hashKeys) {
        return null;
    }

    @Override
    public Boolean mapHasKey(String key, String hashKey) {
        return null;
    }

    @Override
    public <K, V> V mapGet(String key, String hashKey) {
        return null;
    }

    @Override
    public <K, V> List<V> mapMultiGet(String key, Collection<String> hashKeys) {
        return null;
    }

    @Override
    public <K, V> Set<K> mapKeys(String key) {
        return null;
    }

    @Override
    public <K, V> Long mapLengthOfValue(String key, String hashKey) {
        return null;
    }

    @Override
    public Long mapSize(String key) {
        return null;
    }

    @Override
    public <K, V> void mapPutAll(String key, Map<? extends K, ? extends V> m) {

    }

    @Override
    public <K, V> void mapPut(String key, String hashKey, V value) {

    }

    @Override
    public <K, V> Boolean mapPutIfAbsent(String key, String hashKey, V value) {
        return null;
    }

    @Override
    public <K, V> List<V> mapValues(String key) {
        return null;
    }

    @Override
    public <K, V> Map<K, V> mapEntries(String key) {
        return null;
    }

    @Override
    public void valueSet(String key, Object value) {
        synchronized (key){
            memoryCache.put(key, value);
        }
    }

    @Override
    public void valueSet(String key, Object value, long timeout, TimeUnit unit) {
        synchronized (key){
            memoryCache.putExpire(key, value, timeout, unit);
        }
    }

    @Override
    public Boolean valueSetIfAbsent(String key, Object value) {
        boolean flag = false;
        synchronized (key) {
            if (!memoryCache.contains(key)){
                memoryCache.put(key, value);
                flag = true;
            }
        }
        return flag;
    }

    @Override
    public Boolean valueSetIfAbsent(String key, Object value, long timeout, TimeUnit unit) {
        boolean flag = false;
        synchronized (key) {
            if (!memoryCache.contains(key)){
                flag = memoryCache.putExpire(key, value, timeout, unit);
            }
        }
        return flag;
    }

    @Override
    public Boolean valueSetIfPresent(String key, Object value) {
        boolean flag = false;
        synchronized (key){
            if (memoryCache.contains(key)){
                memoryCache.put(key, value);
                flag = true;
            }
        }
        return flag;
    }

    @Override
    public Boolean valueSetIfPresent(String key, Object value, long timeout, TimeUnit unit) {
        boolean flag = false;
        synchronized (key){
            if (memoryCache.contains(key)){
                flag = memoryCache.putExpire(key, value, timeout, unit);
            }
        }
        return flag;
    }

    @Override
    public void valueMultiSet(Map<String, Object> map) {
        Assert.notNull(map, "map not null");

        for (Map.Entry<String, Object> item :  map.entrySet()) {
            valueSet(item.getKey(), item.getValue());
        }
    }

    @Override
    public Boolean valueMultiSetIfAbsent(Map<String, Object> map) {
        Assert.notNull(map, "map not null");

        for (Map.Entry<String, Object> item :  map.entrySet()) {
            valueSetIfAbsent(item.getKey(), item.getValue());
        }

        return true;
    }

    @Override
    public <T> T valueGet(String key) {
        return (T) memoryCache.get(key);
    }

    @Override
    public <T> T getAndSet(String key, T value) {
        synchronized (key){
            return (T) memoryCache.put(key, value);
        }
    }

    @Override
    public <V> List<V> multiGet(Collection<String> keys) {
        Assert.notNull(keys, "keys not null");

        if (keys.isEmpty()){
            return Collections.emptyList();
        }

        List<V> values = new LinkedList<>();
        for (String item : keys){
            values.add(valueGet(item));
        }

        return values;
    }

    private AtomicLong getAtomicLong(String key){
        synchronized (key){
            AtomicLong atomicLong = valueGet(key);
            if (atomicLong == null){
                atomicLong = new AtomicLong();
                valueSet(key, atomicLong);
            }

            return atomicLong;
        }
    }

    @Override
    public Long increment(String key) {
        AtomicLong atomicLong = getAtomicLong(key);
        return atomicLong.getAndIncrement();
    }

    @Override
    public Long increment(String key, long delta) {
        AtomicLong atomicLong = getAtomicLong(key);
        return atomicLong.addAndGet(delta);
    }

    @Override
    public Long decrement(String key) {
        AtomicLong atomicLong = getAtomicLong(key);
        return atomicLong.decrementAndGet();
    }

    @Override
    public Long decrement(String key, long delta) {
        return increment(key, delta * -1);
    }

    @Override
    public Integer append(String key, String value) {
        String oldVal = null;

        synchronized (key){
            oldVal = valueGet(key);
            if (oldVal == null){
                valueSet(key, value);

            } else {
                value = oldVal + value;
                valueSet(key, value);
            }
        }

        return value.length();
    }

    @Override
    public Boolean expire(String key, long timeout, TimeUnit unit) {
        return memoryCache.expire(key, timeout, unit);
    }

    @Override
    public Boolean delete(String key) {
        synchronized (key){
            memoryCache.remove(key);
        }
        return true;
    }

    @Override
    public Long delete(Collection<String> keys) {
        Assert.isNull(keys, "keys not null");
        for (String item : keys) {
            delete(item);
        }

        return new Long(keys.size());
    }

    @Override
    public boolean contains(String key) {
        return memoryCache.contains(key);
    }

    @Override
    public <T> List<T> extendCommand(ExecBatch<T> execBatch) {
        T t = execBatch.exec(null);
        List<T> value = new LinkedList<>();
        value.add(t);

        return value;
    }
}
