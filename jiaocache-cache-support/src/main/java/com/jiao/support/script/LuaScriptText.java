package com.jiao.support.script;

/**
 * @Description
 * @Author Vincent.jiao
 * @Date 2022/5/20 14:31
 */
public class LuaScriptText {
    /**
     * bit 根据区间获取存在的位集合.
     * @return
     */
    public static String getBitRangeScriptText() {
        return  " local getIdRange = function (key, min, max) \n" +
                "    local existsArr = {}; \n" +
                "    local idx = 0; \n" +
                "    for i = tonumber(min), tonumber(max) do\n" +
                "        if (redis.call('GETBIT', key, i) == 1)\n" +
                "        then\n" +
                "            idx = idx + 1;\n" +
                "            existsArr[idx] = i;\n" +
                "        end;\n" +
                "    end;\n" +
                "    return existsArr;\n" +
                " end; \n" +

                " local dataJson = cjson.decode(KEYS[1]); " +
                " local key = dataJson[1]; " +
                " local min = dataJson[2]; " +
                " local max = dataJson[3]; " +
                "return getIdRange(key, min, max); ";
    }
}





















