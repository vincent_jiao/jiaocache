package com.jiao.support.rule;

import java.util.Collection;
import java.util.List;

/**
 * 队列缓存支持
 * @Author: vincent.jiao
 * @Date: 2021/4/18
 */
public interface QueueCacheSupport  {
    /**
     * 获取元素之间begin，并end从列表中key.
     * @param key
     * @param start
     * @param end
     * @return
     */
    <E> List<E> queueRange(String key, long start, long end);

    /**
     * 返回队列大小.
     * @param key
     * @return
     */
    Long queueSize(String key);

    /**
     * 添加到表头(压栈).
     * @param key
     * @param value
     * @return
     */
    <E> Long queueLeftPush(String key, E value);

    /**
     * 添加到表头(压栈).
     * @param key
     * @param values 数组
     * @return
     */
    <E> Long queueLeftPushAll(String key, E... values);

    /**
     * 添加到表头(压栈).
     * @param key
     * @param var2 集合
     * @return
     */
    <E> Long queueLeftPushAll(String key, Collection<E> var2);

    /**
     * 添加到表头(压栈) 只有在列表中存在.
     * @param key
     * @param var2
     * @return
     */
    <E> Long queueLeftPushIfPresent(String key, E var2);

    /**
     * 添加到队尾.
     * @param key
     * @param var2
     * @return
     */
    <E> Long queueRightPush(String key, E var2);

    /**
     * 添加到队尾.
     * @param key
     * @param e
     * @return
     */
    <E> Long queueRightPushAll(String key, E... e);

    /**
     * 添加到队尾.
     * @param key
     * @param e
     * @return
     */
    <E> Long queueRightPushAll(String key, Collection<E> e);

    /**
     * 添加到队尾 只有在列表中存在.
     * @param key
     * @param e
     * @return
     */
    <E> Long queueRightPushIfPresent(String key, E e);

    /**
     * 将元素设置到指定索引位置.
     * @param key
     * @param index 索引位置
     * @param e
     */
    <E> void queueSet(String key, long index, E e);

    /**
     * 从表头开始删除 count 个指定的 value，
     * @param key
     * @param count 个数
     * @param value
     * @return
     */
    Long queueRemove(String key, long count, Object value);

    /**
     * 返回指定的索引值
     * @param key
     * @param index
     * @return
     */
    <E> E queueIndex(String key, long index);

    /**
     * 删除并返回存储在中的列表中的第一个元素key.
     * @param key
     * @return
     */
    <E> E queueLeftPop(String key);

    /**
     * 删除并返回存储在中的列表中的最后一个元素key.
     * @param key
     * @return
     */
    <E> E queueRightPop(String key);

}
