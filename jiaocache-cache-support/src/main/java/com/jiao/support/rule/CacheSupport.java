package com.jiao.support.rule;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 用于继承所有缓存类型操作的接口.
 * @Author: vincent.jiao
 * @Date: 2021/6/8
 */
public interface CacheSupport extends SetCacheSupport, MapCacheSupport,
        ListCacheSupport, QueueCacheSupport, BitMapCacheSupport, ValueCacheSupport {

    /**
     * 设定过期时间
     * @param key
     * @param timeout
     * @param unit
     * @return
     */
    public abstract Boolean expire(String key, long timeout, TimeUnit unit);

    /**
     * 删除key
     * @param key
     * @return
     */
    public abstract Boolean delete(String key);

    /**
     * 删除集合 key
     * @param keys
     * @return
     */
    public abstract Long delete(Collection<String> keys);

    /**
     * 是否存在 key
     * @param key
     * @return
     */
    public abstract boolean contains(String key);

    /**
     * 扩展命令.
     * 内部委托给 execBatch 去执行
     * @param execBatch
     * @param <T>
     * @return
     */
    public abstract <T> List<T> extendCommand(ExecBatch<T> execBatch);
}
