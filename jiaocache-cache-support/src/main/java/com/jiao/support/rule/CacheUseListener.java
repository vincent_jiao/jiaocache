package com.jiao.support.rule;


/**
 * 缓存切换监听器.
 * 监听器必须注册到 spring bean管理中，否则无法自动加入监听集合中
 * <p>
 *     用于本地>远程，远程>本地 切换时候的逻辑处理，比如本地缓存切换到远程缓存时候的一些数据合并
 * </p>
 * @Author: vincent.jiao
 * @Date: 2021/4/17
 */
public interface CacheUseListener {
    public void eventUseLocalCache(CacheSupport localSupport, CacheSupport remoteCache);
    public void eventUseRemoteCache(CacheSupport localSupport, CacheSupport remoteCache);
}
