package com.jiao.support.rule;

import java.util.Collection;

/**
 * 基于 bitmap.
 * @Author: vincent.jiao
 * @Date: 2021/4/18
 */
public interface BitMapCacheSupport {
    /**
     * 指定偏移量设置值.
     * @param offset
     * @param flag
     */
    public abstract void bitSetBit(String key, long offset, boolean flag);

    /**
     * 返回偏移量值.
     * @param offset
     * @return
     */
    public abstract boolean bitGetBit(String key, long offset);


    /**
     * 返回存在的指定 范围偏移量值集合.
     * @param min 范围下限
     * @param max 范围上限
     * @return
     */
    public abstract Collection<Long> bitGetExistsBit(String key, long min, long max);

    /**
     * 返回存在的指定 偏移量值集合.
     * @param offsets 偏移量集合
     * @return
     */
    public abstract Collection<Long> bitGetExistsBit(String key, Collection<Long> offsets);

    /**
     * 返回个数.
     * @return
     */
    public abstract Long bitGetCount(String key);

}
