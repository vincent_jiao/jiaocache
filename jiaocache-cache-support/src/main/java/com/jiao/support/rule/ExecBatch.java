package com.jiao.support.rule;

/**
 * @Author: vincent.jiao
 * @Date: 2021/5/7
 */
public interface ExecBatch<T> {
    public T exec(Object... args) ;
}
