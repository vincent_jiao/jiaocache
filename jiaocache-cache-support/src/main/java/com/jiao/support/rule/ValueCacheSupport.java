package com.jiao.support.rule;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Author: vincent.jiao
 * @Date: 2021/5/6
 */
public interface ValueCacheSupport {


    /**
     * 设置value为key
     * @param key
     * @param value
     */
    void valueSet(String key, Object value);

    /**
     * 设置value和的到期timeout时间key.
     * @param key must not be {@literal null}.
     * @param value must not be {@literal null}.
     * @param timeout the key expiration timeout.
     * @param unit must not be {@literal null}.
     */
    void valueSet(String key, Object value, long timeout, TimeUnit unit);

    /**
     * 如果key不存在，设置key来保存字符串值.
     *
     * @param key must not be {@literal null}.
     * @param value must not be {@literal null}.
     * @return {@literal null} when used in pipeline / transaction.
     */
    Boolean valueSetIfAbsent(String key, Object value);

    /**
     * 如果key不存在，设置key来保存字符串值.
     *
     * @param key must not be {@literal null}.
     * @param value must not be {@literal null}.
     * @param timeout the key expiration timeout.
     * @param unit must not be {@literal null}.
     * @return {@literal null} when used in pipeline / transaction.
     */
    Boolean valueSetIfAbsent(String key, Object value, long timeout, TimeUnit unit);

    /**
     * 如果key存在，设置key来保存字符串值.
     * @param key must not be {@literal null}.
     * @param value must not be {@literal null}.
     * @return command result indicating if the key has been set.
     */
    Boolean valueSetIfPresent(String key, Object value);

    /**
     * 如果key存在，设置key来保存字符串值.
     * @param key must not be {@literal null}.
     * @param value must not be {@literal null}.
     * @param timeout the key expiration timeout.
     * @param unit must not be {@literal null}.
     * @return command result indicating if the key has been set.
     * @since 2.1
     */
    Boolean valueSetIfPresent(String key, Object value, long timeout, TimeUnit unit);

    /**
     * 设置多个键值.
     * @param map must not be {@literal null}.
     * @see <a href="https://redis.io/commands/mset">Redis Documentation: MSET</a>
     */
    void valueMultiSet(Map<String, Object> map);

    /**
     * 仅当提供的键不存在时，设置多个键值.
     *
     * @param map must not be {@literal null}.
     * @param {@literal null} when used in pipeline / transaction.
     */
    Boolean valueMultiSetIfAbsent(Map<String, Object> map);

    /**
     * 返回值.
     * @param key must not be {@literal null}.
     * @return {@literal null} when used in pipeline / transaction.
     */
    <T> T valueGet(String key);

    /**
     * 设置key的value并返回它的旧值。.
     * @param key must not be {@literal null}.
     * @return {@literal null} when used in pipeline / transaction.
     */
    <T> T getAndSet(String key, T value);

    /**
     * 得到多个键。值将按请求的键的顺序返回.
     * @param keys must not be {@literal null}.
     * @return {@literal null} when used in pipeline / transaction.
     */
    <V> List<V>  multiGet(Collection<String> keys);

    /**
     * 将作为字符串值存储在键下的整数值增加1.
     * @param key must not be {@literal null}.
     * @return {@literal null} when used in pipeline / transaction.
     */
    Long increment(String key);

    /**
     * 递增指定值.
     *  //TODO 待确认api用法
     * @param key must not be {@literal null}.
     * @param delta
     * @return {@literal null} when used in pipeline / transaction.
     */
    Long increment(String key, long delta);

    /**
     * 递减.
     * @param key must not be {@literal null}.
     * @return {@literal null} when used in pipeline / transaction.
     * @since 2.1
     */
    Long decrement(String key);

    /**
     * //TODO 待确认api用法
     * Decrement an integer value stored as string value under {@code key} by {@code delta}.
     * @param key must not be {@literal null}.
     * @param delta
     * @return {@literal null} when used in pipeline / transaction.
     */
    Long decrement(String key, long delta);

    /**
     * Append a {@code value} to {@code key}.
     * //TODO 待确认api用法
     * @param key must not be {@literal null}.
     * @param value
     * @return {@literal null} when used in pipeline / transaction.
     */
    Integer append(String key, String value);
}
