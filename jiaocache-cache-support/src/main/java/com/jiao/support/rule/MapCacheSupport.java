package com.jiao.support.rule;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * map 数据结构支持
 * @Author: vincent.jiao
 * @Date: 2021/4/18
 */
public interface MapCacheSupport{
    /**
     * 删除指定 hash key
     * @param key
     * @param hashKeys
     * @return
     */
    Long mapDelete(String key, String... hashKeys);

    /**
     * 是否存在 hashkey
     * @param key
     * @param hashKey
     * @return
     */
    Boolean mapHasKey(String key, String hashKey);

    /**
     * 返回指定 hashkey 对应的数据
     * @param key
     * @param hashKey
     * @return
     */
    <K, V> V mapGet(String key, String hashKey);

    /**
     * 返回指定 hashkey 集合对应的数据
     * @param key
     * @param hashKeys
     * @return
     */
    <K, V> List<V> mapMultiGet(String key, Collection<String> hashKeys);

    /**
     * 返回指定 key 对应所有的 hashkey
     * @param key
     * @return
     */
    <K, V> Set<K> mapKeys(String key);

    /**
     * 返回指定 hashkey 对应的长度
     * @param key
     * @param hashKey
     * @return
     */
    <K, V> Long mapLengthOfValue(String key, String hashKey);

    /**
     * 返回 hash 表的长度
     * @param key
     * @return
     */
    Long mapSize(String key);

    /**
     * 插入集合数据.
     * @param key
     * @param m
     */
    <K, V> void mapPutAll(String key, Map<? extends K, ? extends V> m);

    /**
     * 插入单个数据.
     * @param key
     * @param hashKey
     * @param value
     */
    <K, V>  void mapPut(String key, String hashKey, V value);

    /**
     * 不存 hashkey 的时候设置
     * @param key
     * @param hashKey
     * @param value
     * @return
     */
    <K, V> Boolean mapPutIfAbsent(String key, String hashKey, V value);

    /**
     * 返回哈希表所有值的集合
     * @param key
     * @return
     */
    <K, V> List<V> mapValues(String key);

    /**
     * 返回哈希表
     * @param key
     * @return
     */
    <K, V> Map<K, V> mapEntries(String key);

}
