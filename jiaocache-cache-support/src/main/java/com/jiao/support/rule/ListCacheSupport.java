package com.jiao.support.rule;

import java.util.Collection;

/**
 * @Author: vincent.jiao
 * @Date: 2021/4/18
 */
public interface ListCacheSupport {
    int listSize(String key);

    boolean listContains(String key, Object o);

    <E> boolean listAdd(String key, E e);

    <E>  boolean listaddAll(String key, Collection<? extends E> c);

    <E> E listGet(String key, int index);

    <E>  void listSetByIndex(String key, int index, E element);

    <E> E listRemove(String key, int index);

}
