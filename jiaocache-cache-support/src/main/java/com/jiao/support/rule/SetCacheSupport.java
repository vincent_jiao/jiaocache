package com.jiao.support.rule;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @Author: vincent.jiao
 * @Date: 2021/4/18
 */
public interface SetCacheSupport {
    /**
     * 将指定的数据加入到 set 中
     * @param key
     * @param values
     * @return
     */
    <E> Long setAdd(String key, E... values);

    /**
     * 将指定的数据从 set 中删除
     * @param key
     * @param values
     * @return
     */
    Long setRemove(String key, Object... values);

    /**
     * 随机移除一个值，并返回.
     * @param key
     * @return
     */
    <E> E setPop(String key);

    /**
     * 随机移除 count 个值，并返回.
     * @param key
     * @param count
     * @return
     */
    <E> List<E> setPop(String key, long count);

    /**
     * 将值从一个键移到另一个键.
     * @param key
     * @param value
     * @param destKey
     * @return
     */
    <E> Boolean setMove(String key, E value, String destKey);

    /**
     * 返回大小
     * @param key
     * @return
     */
    Long size(String key);

    /**
     * 检查 set 中字符包含某个值
     * @param key
     * @param o
     * @return
     */
    boolean setContains(String key, Object o);

    /**
     * 返回在 key 和指定 otherKey 相交的 set 集合.
     * @param key
     * @param otherKey
     * @return
     */
    <E> Set<E> setIntersect(String key, String otherKey);

    /**
     * 返回在 key 和指定所有 otherKey 集合相交的 set 集合.
     * @param key
     * @param otherKeys
     * @return
     */
    <E> Set<E> setIntersect(String key, Collection<String> otherKeys);

    /**
     * 返回指定key集合中，所有相交的数据集合
     * @param keys
     * @return
     */
    <E> Set<E> setIntersect(Collection<String> keys);

    /**
     * 返回 key 合并 otherKey 数据（去重）
     * @param key
     * @param otherKey
     * @return
     */
    <E> Set<E> setUnion(String key, String otherKey);

    /**
     * 返回 key 合并 otherKey 集合key中集合数据（去重）
     * @param key
     * @param otherKeys
     * @return
     */
    <E> Set<E> setUnion(String key, Collection<String> otherKeys);

    /**
     * 返回 keys 合并集合数据（去重）
     * @param keys
     * @return
     */
    <E> Set<E> setUnion(Collection<String> keys);

    /**
     * 返回 key 与 otherKey 差异的数据（差集）
     * @param key
     * @param otherKey
     * @return
     */
    <E> Set<E> setDifference(String key, String otherKey);

    /**
     * 返回 key 与 otherKey 集合key中差异的数据（差集）
     * @param key
     * @param otherKeys
     * @return
     */
    <E> Set<E> setDifference(String key, Collection<String> otherKeys);

    /**
     * 返回 keys 差集集合数据
     * @param keys
     * @return
     */
    <E> Set<E> setDifference(Collection<String> keys);

    /**
     * 获取set的所有元素ke.
     * @param key
     * @return
     */
    <E> Set<E> setgetAll(String key);

    /**
     * 从处获取随机元素key.
     * @param key
     * @return
     */
    <E> E setRandomMember(String key);

    /**
     * 获取 count 个不同的随机数
     * @param key
     * @param count
     * @return
     */
    <E> Set<E> setDistinctRandomMembers(String key, long count);

    /**
     * 获取 count 个随价数（可能存在相同）
     * @param key
     * @param count
     * @return
     */
    <E> List<E> setRandomGet(String key, long count);
}
