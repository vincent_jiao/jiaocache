package com.jiao.autoconfig.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DatabaseDriver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import javax.sql.DataSource;

/**
 * @Description
 * @Author Vincent.jiao
 * @Date 2022/5/26 11:21
 */
@Configuration(proxyBeanMethods = false)
@ConditionalOnMissingBean(DataSource.class)
//@ConditionalOnProperty(name = "cache.system.type")
public class CacheDataSourceConfiguration {
    @SuppressWarnings("unchecked")
    protected static <T> T createDataSource(CacheDataSourceProperties properties, Class<? extends DataSource> type) throws ClassNotFoundException {
        return (T) properties.initializeDataSourceBuilder().type(type).build();
    }

    /**
     * Generic DataSource configuration.
     */
    @Bean
    DataSource dataSource(CacheDataSourceProperties properties) throws ClassNotFoundException {
        return properties.initializeDataSourceBuilder().build();
    }
}
