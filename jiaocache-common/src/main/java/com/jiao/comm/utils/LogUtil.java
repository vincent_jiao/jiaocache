package com.jiao.comm.utils;

import cn.hutool.core.util.StrUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 日志记录.
 * 快速获取调用者的日志记录器, 简化代码.
 * @Author Vincent.jiao
 * @Date 2022/3/29 9:24
 */
public class LogUtil {
    /**
     * 当前默认的日志记录器.
     */
    public static String thisClassName = LogUtil.class.getName();

    public static Map<Class, Logger> loggerMap = new ConcurrentHashMap<>();

    /**
     * 获取调用类的日志记录器.
     * @return
     */
    public static Logger getLogger() {
        //获取调用此方法的类
        StackTraceElement stacks[] = Thread.currentThread().getStackTrace();
        Class invoker = null;
        for (int i = 1; i < stacks.length; i++) {
            if(!StrUtil.equalsIgnoreCase(stacks[i].getClassName(), thisClassName)) {
                try {
                    invoker = Class.forName(stacks[i+1].getClassName());
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                break;
            }
        }

        if(invoker == null) {
            invoker = LogUtil.class;
        }

        Logger logger  = loggerMap.get(invoker);
        if(logger == null) {
            logger = LoggerFactory.getLogger(invoker);
            loggerMap.put(invoker, logger);
        }

        return logger;
    }


    public static boolean isDebugEnabled() {
        return getLogger().isDebugEnabled();
    }

    public static void debug(String msg) {
        getLogger().debug(msg);
    }

    public static void debug(String format, Object arg) {
        getLogger().debug(format, arg);
    }

    public static void debug(String format, Object arg1, Object arg2) {
        getLogger().debug(format, arg1, arg2);
    }

    public void debug(String format, Object... arguments) {
        getLogger().debug(format, arguments);
    }

    
    public static void debug(String msg, Throwable t) {
        getLogger().debug(msg, t);
    }

    
    public static void info(String msg) {
        getLogger().info(msg);
    }

    
    public static void info(String format, Object arg) {
        getLogger().info(format, arg);
    }

    
    public static void info(String format, Object arg1, Object arg2) {
        getLogger().info(format, arg1, arg2);
    }

    
    public static void info(String format, Object... arguments) {
        getLogger().info(format, arguments);
    }

    
    public static void info(String msg, Throwable t) {
        getLogger().info(msg, t);
    }


    
    public void warn(String msg) {
        getLogger().warn(msg);
    }

    
    public static void warn(String format, Object arg) {
        getLogger().warn(format, arg);
    }

    
    public static void warn(String format, Object... arguments) {
        getLogger().warn(format, arguments);
    }

    
    public static void warn(String format, Object arg1, Object arg2) {

    }

    
    public static void warn(String msg, Throwable t) {
        getLogger().warn(msg, t);
    }


    public static void error(String msg) {
        getLogger().error(msg);
    }

    
    public static void error(String format, Object arg) {
        getLogger().error(format, arg);
    }


    public static void error(String format, Object arg1, Object arg2) {
        getLogger().error(format, arg1, arg2);
    }

    
    public static void error(String format, Object... arguments) {
        getLogger().error(format, arguments);
    }

    
    public static void error(String msg, Throwable t) {
        getLogger().error(msg, t);
    }
}
