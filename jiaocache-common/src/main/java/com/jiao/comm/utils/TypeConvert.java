package com.jiao.comm.utils;

import cn.hutool.core.collection.CollectionUtil;

import java.util.*;

/**
 * @Author : Vincent.jiao
 * @Date : 2022/5/21 11:27
 * @Version : 1.0
 */
public class TypeConvert {
    public static Long stringToLong(String s) {
        return Long.parseLong(s);
    }

    public static Long objectToLong(Object obj) {
        if(obj == null) {
            return null;
        }

        if(obj instanceof Long) {
            return (Long) obj;
        }

        Long l = null;
        if(obj instanceof String) {
            l = stringToLong(obj.toString());
        } else if(obj instanceof Number) {
            l = ((Number) obj).longValue();
        }

        return l;
    }

    public static <T> T collectionToLong(Collection collection) {
        if(collection instanceof List) {
            return (T) listToLong((List) collection);
        } else if(collection instanceof Set) {
            return (T) setToLong((Set) collection);
        }

        return null;
    }

    public static List<Long> listToLong(List list) {
        if(CollectionUtil.isEmpty(list)) {
            return Collections.emptyList();
        }

        List<Long> result = new LinkedList<>();
        for (Object item : list){
            result.add(objectToLong(item));
        }

        return result;
    }

    public static Set<Long> setToLong(Set set) {
        if(CollectionUtil.isEmpty(set)) {
            return Collections.emptySet();
        }

        Set<Long> result = new TreeSet<>();
        for (Object item : set){
            result.add(objectToLong(item));
        }

        return result;
    }
}
