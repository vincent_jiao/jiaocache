package com.jiao.comm.utils;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @Author: vincent.jiao
 * @Date: 2021/5/9
 */
public class CollectionNullUtils {
    public static <T> Set<T> getSet(Set<T> c){
        return c == null ? Collections.emptySet() : c;
    }

    public  static <T> List<T> getList(List<T> c){
        return c == null ? Collections.emptyList() : c;
    }

    public  static <K, V> Map<K, V> getList(Map<K, V> c){
        return c == null ? Collections.emptyMap() : c;
    }

    public  static  boolean isEmpty(Collection c){
        return c == null || c.isEmpty();
    }
}
