package com.jiao.comm.utils;

import cn.hutool.core.util.ClassUtil;

import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

/**
 * @Description
 * @Author Vincent.jiao
 * @Date 2022/5/16 16:02
 */
public class ClassUtills extends ClassUtil {
    public static List<Method> getMethodList(Class clazz, String methodName){
        List<Method> methods = new LinkedList<>();
        for (Method item : getDeclaredMethods(clazz) ){
            if(item.getName().equalsIgnoreCase(methodName)) {
                methods.add(item);
            }
        }

        return methods;
    }
}
