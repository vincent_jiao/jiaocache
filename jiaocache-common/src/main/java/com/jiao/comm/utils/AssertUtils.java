package com.jiao.comm.utils;


import com.jiao.comm.exception.CacheSysException;

/**
 * 断言判断.
 * @Author Vincent.jiao
 * @Date 2022/3/18 13:49
 */
public class AssertUtils {
    public static void check(boolean flag, CacheSysException e) {
        if(flag){
            throw e;
        }
    }
}
