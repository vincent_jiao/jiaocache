package com.jiao.comm.utils;


import lombok.SneakyThrows;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * Java反射工具类.
 *
 * @Author: vincent.jiao
 * @Date: 2021/4/24
 */
public class ReflectUtil {
    /**
     * 根据字段名字段值.
     * @return 所有属性名
     */
    @SneakyThrows
    public static Object getFieldValueByName(Class classObj, String name, Object o) {
        Field field = classObj.getDeclaredField(name);
        if(field == null){
            return null;
        }

        return field.get(o);
    }

    @SneakyThrows
    public static Annotation getAnnotation(Class classObj, Class annoClass) {
        return classObj.getAnnotation(annoClass);
    }
}
