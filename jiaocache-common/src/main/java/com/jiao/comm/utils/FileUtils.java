package com.jiao.comm.utils;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.core.io.ClassPathResource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

/**
 * @Author : Vincent.jiao
 * @Date : 2022/5/22 10:37
 * @Version : 1.0
 */
public class FileUtils {
    /**
     * 返回jar内的文件
     * @param res
     * @return
     */
    public static String getJarFileText(String res) throws IOException {
        List<String> allNameList = FileUtils.getJarResource("redis");
        return StrUtil.join("\n", allNameList);
    }

    public static List<String> getJarResource(String res) throws IOException {
        List<String> names = new LinkedList<>();
        ClassPathResource resource = new ClassPathResource(res);
        try (
                InputStream inputStream = resource.getInputStream();
                InputStreamReader read =  new InputStreamReader(inputStream, "utf-8");
                BufferedReader bufferedReader = new BufferedReader(read);
        ){
            String txt = null;
            while ((txt = bufferedReader.readLine()) != null) {
                if (StrUtil.isNotEmpty(txt)) {
                    names.add(txt);
                    System.out.println(txt);
                }
            }
        } catch (Exception e) {
            throw e;
        }

        return names;
    }
}
