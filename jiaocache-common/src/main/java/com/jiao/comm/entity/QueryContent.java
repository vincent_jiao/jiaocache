package com.jiao.comm.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 定义查询体.
 * @author Vincent.jiao  
 * @date 2019年5月8日
 */
public interface QueryContent<T> {
	public T exectu(ResultSet rs) throws SQLException;
}
