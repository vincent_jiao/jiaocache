package com.jiao.comm.exception;

/**
 * 读取缓存信息异常.
 * @Author: vincent.jiao
 * @Date: 2021/5/3
 */
public class ReaderCacheInfoException extends CacheSysException {
    public ReaderCacheInfoException() {
        super();
    }
    public ReaderCacheInfoException(String m) {
        super("读取缓存信息时，出现异常。" + m);
    }

    public ReaderCacheInfoException(String m, Throwable cause) {
        super(m, cause);
    }
}
