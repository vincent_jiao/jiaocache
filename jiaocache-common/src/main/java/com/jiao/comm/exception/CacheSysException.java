package com.jiao.comm.exception;

/**
 * 缓存系统异常.
 * @Author: vincent.jiao
 * @Date: 2021/5/3
 */
public class CacheSysException extends RuntimeException {
    public CacheSysException() {
        super();
    }
    public CacheSysException(String m) {
        super(m);
    }
    public CacheSysException(String m, Throwable cause) {
        super(m, cause);
    }

}
