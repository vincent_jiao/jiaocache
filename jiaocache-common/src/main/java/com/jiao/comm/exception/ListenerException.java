package com.jiao.comm.exception;

/**
 * @Description
 * @Author Vincent.jiao
 * @Date 2022/5/18 9:35
 */
public class ListenerException extends CacheSysException{
    public ListenerException() {
        super();
    }
    public ListenerException(String m) {
        super(m);
    }
    public ListenerException(String m, Throwable cause) {
        super(m, cause);
    }
}
