package com.jiao.comm.exception;

/**
 * @Author: vincent.jiao
 * @Date: 2021/6/1
 */
public class LuaSyntaxException extends CacheSysException {
    public LuaSyntaxException(){
        super("处理 lua 语法时出现异常");
    }

    public LuaSyntaxException(String msg){
        super(msg);
    }
}
