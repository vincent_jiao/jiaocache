package com.jiao.comm.exception;

/**
 * @Author: vincent.jiao
 * @Date: 2021/5/9
 */
public class DataIncompleteException extends CacheSysException {
    public DataIncompleteException(){
        super("索引数据不完整");
    }

    public DataIncompleteException(String msg){
        super(msg);
    }
}
