package com.jiao.comm.exception;

/**
 * @Author: vincent.jiao
 * @Date: 2021/5/9
 */
public class ConcurrentWriteException extends RuntimeException {
    public ConcurrentWriteException(){
        super("读取数据时候发生并发写，在指定的自旋之后还是未能成功读取数据");
    }

    public ConcurrentWriteException(String msg){
        super(msg);
    }
}
