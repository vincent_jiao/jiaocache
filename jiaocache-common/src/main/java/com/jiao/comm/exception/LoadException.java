package com.jiao.comm.exception;

/**
 * @Author: vincent.jiao
 * @Date: 2021/6/12
 */
public class LoadException extends CacheSysException {
    public LoadException(){
        super("加载异常");
    }

    public LoadException(String msg){
        super(msg);
    }

    public LoadException(String msg,  Throwable cause){
        super(msg, cause);
    }
}
