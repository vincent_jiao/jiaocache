package com.jiao.comm.exception;

/**
 * @Description
 * @Author Vincent.jiao
 * @Date 2022/5/20 10:56
 */
public class ResultMappingException extends CacheSysException {
    public ResultMappingException() {
        super();
    }
    public ResultMappingException(String m) {
        super("结果映射时出现异常. " + m);
    }

    public ResultMappingException(String m, Throwable cause) {
        super(m, cause);
    }
}