package com.jiao.comm.exception;

/**
 * @Description
 * @Author Vincent.jiao
 * @Date 2022/5/18 9:29
 */
public class CloseLoopException extends ListenerException {
    public CloseLoopException() {
        super("在数据闭环时候出现异常");
    }

    public CloseLoopException(String m) {
        super(m);
        startUrgentDeal();
    }

    public CloseLoopException(String m, Throwable cause) {
        super(m, cause);
        startUrgentDeal();
    }

    public void startUrgentDeal() {
        //TODO 待实现紧急处理
    }
}
