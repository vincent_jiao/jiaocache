package com.jiao.comm.exception;

/**
 * @Author: vincent.jiao
 * @Date: 2021/6/7
 */
public class ManyResultsException  extends CacheSysException {
    public ManyResultsException(){
        super("查询到多条数据");
    }

    public ManyResultsException(String msg){
        super(msg);
    }
}
