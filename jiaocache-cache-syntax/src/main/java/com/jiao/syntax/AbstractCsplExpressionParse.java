package com.jiao.syntax;

import cn.hutool.core.util.StrUtil;
import com.jiao.syntax.entity.ExpressionTemplate;
import com.jiao.syntax.entity.Token;
import com.jiao.syntax.impl.ExpressionParse;
import lombok.Getter;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 解析.
 * 将表达式抽象流为 token, 再使用模板（语法树）解析token流
 * @Author: vincent.jiao
 * @Date: 2021/4/19
 */
public abstract class AbstractCsplExpressionParse implements ExpressionParse {
    //表达式串
    @Getter
    protected String expression;

    //列集合
    @Getter
    protected List<String> colunmNameList = new LinkedList<>();

    protected List<Token> tokens;

    protected Tokenizer tokenizer;

    @Getter
    protected String tableName;

    @Getter
    protected String sourceSqlExpression;

    @Getter
    protected ExpressionTemplate expressionTemplate;

    public AbstractCsplExpressionParse(String expression){
        this.expression = expression;
        this.sourceSqlExpression = expression;
    }

    /**
     * 子类应该针对语法进行检查
     */
    protected abstract void parseSyntax();
}
