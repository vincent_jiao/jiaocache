package com.jiao.syntax.exception;

/**
 * @Author: vincent.jiao
 * @Date: 2021/4/19
 */
public class ParseException extends ExpressionException {
    public ParseException() {
        super("表达式解析异常", 0, 0);
    }

    public ParseException(String msg) {
        super(msg);
    }
    private int start;
    private int end;

    public ParseException(String s, int start, int end) {
        super(s, start, end);
        this.start = start;
        this.end = end;
    }
}
