package com.jiao.syntax.exception;

import com.jiao.comm.exception.CacheSysException;

/**
 * 表达式异常.
 * @Author: vincent.jiao
 * @Date: 2021/4/19
 */
public class ExpressionException extends CacheSysException {
    public ExpressionException() {
        super();
    }

    public ExpressionException(String msg) {
        super(msg);
    }

    private int start;
    private int end;

    public ExpressionException(String s, int start, int end) {
        super(s);
        this.start = start;
        this.end = end;
    }

    @Override
    public String getMessage() {
        String s = super.getMessage();
        if(start > 0) {
            s += ", 索引开始位置：" + start;
        }
        if(end > 0){
            s += ", 结束位置：" + end;
        }

        return s;
    }
}
