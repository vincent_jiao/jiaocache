package com.jiao.syntax.exception;

/**
 * @Description
 * @Author Vincent.jiao
 * @Date 2022/6/15 14:02
 */
public class TokenParseException extends ExpressionException {
    public TokenParseException() {
        super("Token解析异常", 0, 0);
    }

    public TokenParseException(String msg) {
        super(msg);
    }
    private int start;
    private int end;

    public TokenParseException(String s, int start, int end) {
        super(s, start, end);
        this.start = start;
        this.end = end;
    }
}
