package com.jiao.syntax.impl;

import com.jiao.comm.exception.CacheSysException;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @Author: vincent.jiao
 * @Date: 2021/4/19
 */
public  interface Expression {
    /**
     * 返回用于创建此表达式的原始字符串（未修改）.
     * @return 原始表达式字符串
     */
    public String getExpressionString();

    /**
     * 返回对应的表名.
     * @return 原始表达式字符串
     */
    public String getExpressionTableName();


    /**
     * 检索表达式并返回结果
     * @return
     * @throws CacheSysException 检索过程中会发生异常，统一声明父类异常，可以捕获后判断是否为对应异常 {@link com.cachesys.cache.exception}
     *
     */
    public <T> T getValue(Object... params) throws CacheSysException;
    /**
     * 检索表达式并返回结果
     * @return
     * @throws CacheSysException 检索过程中会发生异常，统一声明父类异常，可以捕获后判断是否为对应异常 {@link com.cachesys.cache.exception}
     *
     */
    public <T> T getValue(Map<String, Object> params) throws CacheSysException;

    /**
     * 检索表达式并返回结果
     * @return
     * @throws CacheSysException 检索过程中会发生异常，统一声明父类异常，可以捕获后判断是否为对应异常 {@link com.cachesys.cache.exception}
     *
     */
    public <T> List<T> getValueList(Object... params) throws CacheSysException;

    public <T> List<T> getValueList(Map<String, Object> params) throws CacheSysException;
}
