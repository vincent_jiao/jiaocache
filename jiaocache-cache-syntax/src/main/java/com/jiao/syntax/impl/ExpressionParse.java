package com.jiao.syntax.impl;

import com.jiao.syntax.entity.ExpressionTemplate;
import com.jiao.syntax.exception.ParseException;

import java.util.Map;

/**
 * 解析器.
 * @Author: vincent.jiao
 * @Date: 2021/4/19
 */
public interface ExpressionParse {
    /**
     * 解析表达式字符串，并返回得到表达式。可用于重复求值的Expression对象.
     * @param expressionString the raw expression string to parse
     * @return an evaluator for the parsed expression
     * @throws ParseException an exception occurred during parsing
     */
    public ExpressionTemplate parseExpression(String expressionString) throws ParseException ;
}
