package com.jiao.syntax;

import com.jiao.syntax.entity.Token;

import java.util.List;

/**
 * 前提是日期转为毫秒值
 * in、not in: 只能为数字与字符串
 * between x and x: 只能支持数字
 * >、< : 支持数字、日期
 * =、!= ：支持数字、日期、字符串
 *
 * itm_id = 1
 * and itm_id in (2,3)
 * and itm_title = 'jiao' and itm_title in ( 'jiao1', 'jiao2', 'jiao3' )
 * and itm_id between 1 and 5
 * and itm_id > 5
 * and itm_id < 5
 * and itm_id <= 5
 * and itm_id >= 5
 * and itm_id != 5
 * and (
 *    itm_id = 1 or itm_id = 2
 * )
 * TODO 表达式计算测试
 * @Author: vincent.jiao
 * @Date: 2021/4/19
 */
public class Demo {
    public static void main(String[] arsg){

        Tokenizer tokenizer = new Tokenizer(" select itm_id, itm_title from aeitem where (itmType='MAP' and itmTcrId=1) or itmId in (8,13,?) ");
//        Tokenizer tokenizer = new Tokenizer(" ( " +
//                "( itm_id = 1 and itm_type = 'jiao') " +
//                "or " +
//                " (itm_id = 2 and itm_type = 'jiao2') " +
//                ") ");
//        Tokenizer tokenizer = new Tokenizer(" select aeitem where itm_id = 1\n" +
//                "and itm_id not in (2,3)\n" +
//                "and itm_title = 'jiao' and itm_title in ( 'jiao1', 'jiao2', 'jiao3' )\n" +
//                "and itm_id between 1 and 5\n" +
//                "and itm_id > 5\n" +
//                "and itm_id < 5\n" +
//                "and itm_id <= 5\n" +
//                "and itm_id >= 5\n" +
//                "and itm_id != 5\n" +
//                "and (\n" +
//                "   itm_id = 1 or itm_id = 2\n" +
//                ") ");
        List<Token> list = tokenizer.getTokens();
        for (Token item : list) {
            System.out.println(item.stringValue() +"   "+item.getKind()+ "  "+item.isIdentifier());
        }

        //TODO 多测试解析
         System.out.println("");
    }

    private static void demo1(){
        //1.定义要操作的表达式,使用者更多多的是关注此部分,而不关注具体的解析处理部分,
//        String pstr="3s=2";
        String pstr="itm_id = 1\n" +
                "\" +\n" +
                "                \"and itm_id in (2,3)\\n\" +\n" +
                "                \"and itm_title = 'jiao' and itm_title in ( 'jiao1', 'jiao2', 'jiao3' )\\n\" +\n" +
                "                \"and itm_id between 1 and 5\\n\" +\n" +
                "                \"and itm_id > 5\\n\" +\n" +
                "                \"and itm_id < 5\\n\" +\n" +
                "                \"and itm_id <= 5\\n\" +\n" +
                "                \"and itm_id >= 5\\n\" +\n" +
                "                \"and itm_id != 5\\n\" +\n" +
                "                \"and (\\n\" +\n" +
                "                \"   itm_id = 1 or itm_id = 2\\n\" +\n" +
                "                \")";
        //2.要定义一个表达式的解析器,本次使用的是spel表达式解析器;
//        ExpressionParser parpser = new SpelExpressionParser();
//        //3.使用特定的解析器来处理指定的字符串操作;
//        Expression exp = parpser.parseExpression(pstr);
//        //定义相关的一些环境属性,
//        EvaluationContext context = new StandardEvaluationContext();
//        System.out.println(exp.getValue(context));
    }
}
