package com.jiao.syntax.enums;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * 类型
 * @Author: vincent.jiao
 * @Date: 2021/4/19
 */
public enum TokenKind {
    LITERAL_INT,
    LITERAL_LONG,
    LITERAL_STRING,     //字符串

    IDENTIFIER,         //标识符
    FIELD,              //字段

    SELECT("select"),
    WHERE("where"),
    FROM("from"),

    GE(">="),
    GT(">"),
    LE("<="),
    LT("<"),
    EQ("="),
    NE("!="),
    NOT("!"),
    BETWEEN("between"),
    LIKE("like"),    //模糊搜索
    IN("in"),
    NOTIN("not"),
    OR("or"),
    AND("and"),
    COMMA(","),
    DOT("."),
    LPAREN("("),
    RPAREN(")"),
    PRECOMPILE("?"),        //sql 中的预编译
    PLACEHOLDER_35("#"),        //#占位符预编译
    ALLCOLUNM("*"),        //sql 查询全部

    ;

    @Getter
    final char[] tokenChars;
    final String value;
    private final boolean hasPayload;  // 除了简单的种类之外，这个令牌还有更多的东西吗？

    public static final List<TokenKind> OPERATION_LIST = new ArrayList<TokenKind>(){{
        add(TokenKind.GE);
        add(TokenKind.GT);
        add(TokenKind.LE);
        add(TokenKind.LT);
        add(TokenKind.EQ);
        add(TokenKind.NE);
        add(TokenKind.IN);
        add(TokenKind.NOTIN);
        add(TokenKind.LIKE);
    }};
    public static final List<TokenKind> RELATION_LIST = new ArrayList<TokenKind>(){{
        add(TokenKind.OR);
        add(TokenKind.AND);
    }};
    public static final List<TokenKind> SYMBOL_LIST = new ArrayList<TokenKind>(){{
        add(TokenKind.COMMA);
        add(TokenKind.DOT);
        add(TokenKind.LPAREN);
        add(TokenKind.RPAREN);
    }};
    public static final List<TokenKind> VALUE_LIST = new ArrayList<TokenKind>(){{
        add(TokenKind.LITERAL_INT);
        add(TokenKind.LITERAL_LONG);
        add(TokenKind.LITERAL_STRING);
        add(TokenKind.PRECOMPILE);
        add(TokenKind.PLACEHOLDER_35);
    }};
    public static final List<TokenKind> PRECOMPILE_LIST = new ArrayList<TokenKind>(){{
        add(TokenKind.PRECOMPILE);
        add(TokenKind.PLACEHOLDER_35);
    }};

    private TokenKind(String tokenString) {
        this.value = tokenString;
        this.tokenChars = tokenString.toCharArray();
        this.hasPayload = (this.tokenChars.length == 0);
    }

    private TokenKind() {
        this("");
    }


    @Override
    public String toString() {
        return (name() + (this.tokenChars.length !=0 ? "(" + new String(this.tokenChars) +")" : ""));
    }

    public String getValue(){
        return value;
    }
}
