package com.jiao.syntax.enums;

import lombok.Getter;

/**
 * @Author: vincent.jiao
 * @Date: 2021/5/5
 */
public enum DMLType {
    SELECT("SELECT"),
    DELETE("DELETE"),
    UPDATE("UPDATE"),
    INSERT("INSERT")
    ;

    @Getter
    private String value;
    private DMLType(String value) {
        this.value = value;
    }
}
