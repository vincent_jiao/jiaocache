package com.jiao.syntax.entity;

import lombok.Data;

import java.util.List;

/**
 * @Author : Vincent.jiao
 * @Date : 2021/8/22 12:20
 * @Version : 1.0
 */
@Data
public class ExpressionTemplate {
    private List<CselTemplate> cselTemplates;
    private String tableName;
    private String sourceValue;
}
