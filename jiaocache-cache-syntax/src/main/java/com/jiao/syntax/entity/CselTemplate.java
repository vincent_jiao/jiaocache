package com.jiao.syntax.entity;

import com.jiao.syntax.enums.TokenKind;
import lombok.Data;

import java.util.LinkedList;
import java.util.List;

/**
 * 表达式每项由 字段、运算符、值组成
 * @Author: vincent.jiao
 * @Date: 2021/4/23
 */
@Data
public class CselTemplate {
    /** 运算符 */
    TokenKind operationIdentifier;

    /** 值类型 */
    TokenKind valueType;

    /** 字段名 */
    String field;

    /** 值集合 */
    List values;

    /** 表达式之间关系连接符 */
    TokenKind relationIdentifier;

    /** ) */
    boolean isStartGroup;
    /** ( */
    boolean isEndGroup;

    boolean isGroup;
}
