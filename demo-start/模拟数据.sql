---------------------- 人表， 这个表会加入缓存
CREATE TABLE person
(
    p_id INT PRIMARY KEY AUTO_INCREMENT,
    p_name VARCHAR(100) NOT NULL,
    p_sex INT,
    p_card_id VARCHAR(20) NOT NULL COMMENT '身份证号',
    p_explain VARCHAR(200) NOT NULL COMMENT '备注'
);

-- 批量插入数据
DELETE FROM person;
DROP PROCEDURE IF EXISTS batchData;
DELIMITER $$
CREATE PROCEDURE batchData()
BEGIN
	DECLARE i INT  DEFAULT 1;
	WHILE i < 10000 DO
		INSERT INTO person(p_name, p_sex, p_card_id, p_explain)
		VALUES (CONCAT('jiao', i), i % 2, CONCAT('123456', i), CONCAT('我很帅', i));
		SET i = i + 1;
END WHILE;
COMMIT;
END; $$
DELIMITER ;

CALL batchData();

-- 查看数据
SELECT * FROM person;


------------------- 动物表，这个表不会加入缓存
DROP TABLE animal;
CREATE TABLE animal
(
    a_id INT PRIMARY KEY AUTO_INCREMENT,
    a_name VARCHAR(100) NOT NULL,
    a_age INT,
    a_type VARCHAR(20) NOT NULL COMMENT '品种'
);

INSERT INTO animal(a_name, a_age, a_type) VALUES ( '旺财', 10, '狗' );
INSERT INTO animal(a_name, a_age, a_type) VALUES ( '狗蛋', 11, '狗' );
INSERT INTO animal(a_name, a_age, a_type) VALUES ( '狗剩', 12, '狗' );
INSERT INTO animal(a_name, a_age, a_type) VALUES ( '狗子', 13, '狗' );
INSERT INTO animal(a_name, a_age, a_type) VALUES ( '二狗', 14, '狗' );