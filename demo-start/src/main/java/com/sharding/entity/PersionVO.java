package com.sharding.entity;

import com.jiao.table.annotation.ColunmMapping;
import lombok.Data;

/**
 * @Description
 * @Author Vincent.jiao
 * @Date 2022/5/20 9:22
 */
@Data
public class PersionVO {
    @ColunmMapping(name = "p_id")
    private Long pId;

    @ColunmMapping(name = "p_name")
    private String pName;

    @ColunmMapping(name = "p_sex")
    private Integer pSex;
}
