package com.sharding.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * animal
 * @author 
 */
@Data
public class Animal implements Serializable {
    private Long a_id;

    private String a_name;

    private Integer a_age;

    /**
     * 品种
     */
    private String a_type;

    private static final long serialVersionUID = 1L;
}