package com.sharding.entity;

import cn.hutool.core.util.StrUtil;
import com.jiao.table.annotation.PrimaryKey;
import com.jiao.table.annotation.TableCache;
import com.jiao.table.annotation.TableIndex;
import lombok.Data;

/**
 * @Description
 * @Author Vincent.jiao
 * @Date 2022/5/19 13:57
 */
@Data
//指定这张表对应的表名，类中所有的属性需要与数据库列名一致
@TableCache(tableName = "person")

//指定操作这张表的dao 实现类, 内部会调用一些操作数据的方法
public class Person {
    //标识这个字段为主键
    @PrimaryKey
    private Long p_id;

    private String p_name;

    //加入缓存
    @TableIndex
    private Integer p_sex;

    //将这列加入缓存，以便快速查找
    @TableIndex
    private String p_card_id;
    private String p_explain;

    public String toString(){
        return StrUtil.join("_", p_id, p_name, p_sex, p_card_id, p_explain);
    }

}
