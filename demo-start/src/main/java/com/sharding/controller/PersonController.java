package com.sharding.controller;

import cn.hutool.core.util.RandomUtil;
import com.jiao.datasource.parse.SQLStruct;
import com.jiao.table.cache.DynamicCacheRead;
import com.jiao.table.exception.WriteCacheInfoException;
import com.sharding.entity.PersionVO;
import com.sharding.entity.Person;
import com.sharding.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @Description
 * @Author Vincent.jiao
 * @Date 2022/5/19 14:30
 */
@Controller
@RequestMapping("person")
public class PersonController {
    @Autowired
    PersonService personService;

    @Autowired
    DynamicCacheRead dynamicCacheRead;

    @RequestMapping("delete")
    @ResponseBody
    public Object delete (Long id) {
       return personService.deleteByPrimaryKey(id);
    }

    @RequestMapping("deleteSex")
    @ResponseBody
    public Object deleteSex (Integer sex) {
       return personService.deleteBySex(sex);
    }

    @RequestMapping("update")
    @ResponseBody
    public Object update(Long id) {
        Person person = new Person();
        person.setP_id(id);
        person.setP_name(RandomUtil.randomString(8));
        person.setP_card_id(RandomUtil.randomInt(1000000) + "");
        person.setP_explain(RandomUtil.randomString(10));
        person.setP_sex(RandomUtil.randomInt(2));
        personService.updateByPrimaryKey(person);
        return  person;
    }

    @RequestMapping("insert")
    @ResponseBody
    public Object insert(String name) {
        Person person = new Person();
        person.setP_name(name);
        person.setP_card_id(RandomUtil.randomInt(1000000) + "");
        person.setP_explain(RandomUtil.randomString(10));
        person.setP_sex(RandomUtil.randomInt(2));
       personService.insert(person);
       return person;
    }


    @RequestMapping("insertBatch")
    @ResponseBody
    public Object insertBatch( int count) {
        List<Person> list = new LinkedList<>();
        for (int i = 0; i < count; i++) {
            Person person = new Person( );
            String name = RandomUtil.randomString(8);
            person.setP_name(name);
            person.setP_card_id(RandomUtil.randomInt(1000000) + "");
            person.setP_explain(RandomUtil.randomString(10));
            person.setP_sex(RandomUtil.randomInt(2));
            list.add(person);
        }

        personService.insertBatch(list);
        for (Person p : list){
            System.out.println("insert " + p);
        }

       return list;
    }


    /**
     * 并发写.
     * @return
     */
    @RequestMapping("concurrWrite")
    @ResponseBody
    public Object concurrWrite() {
        List<Person> list = personService.getAll();

        for (int i = 0; i < 3; i++) {
            new Thread(() -> {
                Long id = list.get(RandomUtil.randomInt(list.size() - 1)).getP_id();
                System.out.println("delete id ==>" + id);
                personService.deleteByPrimaryKey(id);
            }).start();
        }

        for (int i = 0; i < 3; i++) {
            new Thread(() -> {
                insertBatch(3);
            }).start();
        }

        for (int i = 0; i < 3; i++) {
            new Thread(() -> {
                Person person = list.get(RandomUtil.randomInt(list.size() - 1));
                String name = RandomUtil.randomString(8);
                System.out.println("update id:"+person.getP_id()+" name ==>" + name);
                person.setP_name(name);

                personService.updateByPrimaryKeySelective(person);
            }).start();
        }

        return "OK";
    }

    @RequestMapping("getById")
    @ResponseBody
    public Object getById(Long id) {
        Object o = null;
//        return dynamicCacheRead.getByPrimaryKey("person", id);
        o = personService.selectByPrimaryKey(id);

        System.out.println(o);
        return o;
    }


        @RequestMapping("getCacheById")
    @ResponseBody
    public Object getCacheById(Long id) {
        Object o = null;
//        return dynamicCacheRead.getByPrimaryKey("person", id);
//        o = dynamicCacheRead.getByPrimaryKey("person", id, PersionVO.class);
        o = dynamicCacheRead.getValueListByCSQL(" select * from person where p_card_id = '1234561'", PersionVO.class);

        System.out.println(o);
        return o;
    }

    @RequestMapping("getResultBycasql")
    @ResponseBody
    public Object getResultBycasql() {
//        return dynamicCacheRead.getValueListByCSQL(" select * from person ");
//        return dynamicCacheRead.getValueListByCSQL(" select * from person ", PersionVO.class);
//        return dynamicCacheRead.getValueListByCSQL(" select * from person where p_id > 300");
//        return dynamicCacheRead.getValueListByCSQL(" select * from person where p_id >= 300 and p_id <= 350 ");
//        return dynamicCacheRead.getValueListByCSQL(" select * from person  where p_id <= 10 and p_sex = 0  or ( p_id between 600 and 610 and p_sex = 0 ) ");
//        return dynamicCacheRead.getValueListByCSQL(" select * from person  where p_id in (1,2,3,4,5,6,7,8,9,50000) " +
//                " and p_sex != 0 ");
//        return dynamicCacheRead.getValueListByCSQL(" select * from person where p_id in (?, ?, ?) or p_id in (?, ?) "
//                , 10, 20, 1, 2, 3);

        //测试占位符
        Map<String, Object> params = new HashMap<>();
        params.put("num1", 1);
        params.put("num2", 2);
        params.put("p_sex", 1);
        params.put("p_card_id", "12345620");
        return dynamicCacheRead.getValueListByCSQL(" select * from person where p_id in (#{num1} , #{num2} ) " +
                " or p_sex = #{p_sex} or p_id between #{num1} and #{num2} or p_card_id = #{p_card_id}", params);
    }

    @RequestMapping("cacheCompensate")
    @ResponseBody
    public Object cacheCompensate() {
        String[] typeArr = {SQLStruct.DELETE, SQLStruct.UPDATE, SQLStruct.INSERT};

        for (int i = 0; i < 10; i++) {
            try{
                String type = typeArr[RandomUtil.randomInt(typeArr.length)];

                Person person = new Person();
                String name = RandomUtil.randomString(8);
                person.setP_id(RandomUtil.randomLong(200));
                person.setP_name(name);
                person.setP_card_id(RandomUtil.randomInt(1000000) + "");
                person.setP_explain(RandomUtil.randomString(10));
                person.setP_sex(RandomUtil.randomInt(2));

                throw new WriteCacheInfoException(person, type);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        return "OK";
    }
}
