package com.sharding.controller;

import cn.hutool.core.util.RandomUtil;
import com.sharding.entity.Animal;
import com.sharding.entity.Person;
import com.sharding.mapper.AnimalMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Description
 * @Author Vincent.jiao
 * @Date 2022/5/19 14:52
 */
@Controller
@RequestMapping("animal")
public class AnimalController {
    @Autowired
    AnimalMapper animalMapper;

    @RequestMapping("update")
    @ResponseBody
    public Object update(Long id) {
        Animal animal = new Animal();
        animal.setA_id(id);

        String name = "狗:" + RandomUtil.randomString(5);
        System.out.println("animal update ==> " + name);
        animal.setA_name(name);
        animal.setA_type("狗");
        animal.setA_age(RandomUtil.randomInt(30));
        animalMapper.updateByPrimaryKey(animal);

        return animal;
    }

    @RequestMapping("insert")
    @ResponseBody
    public Object insert() {
        Animal animal = new Animal();

        String name = "狗:" + RandomUtil.randomString(5);
        System.out.println("animal insert ==> " + name);
        animal.setA_name(name);
        animal.setA_type("狗");
        animal.setA_age(RandomUtil.randomInt(30));
        animalMapper.insert(animal);

        return animal;
    }

}
