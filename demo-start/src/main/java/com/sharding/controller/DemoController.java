package com.sharding.controller;

import cn.hutool.core.util.RandomUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.jiao.support.script.LuaScriptText;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.BitFieldSubCommands;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.ReturnType;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Arrays;
import java.util.List;

import static com.jiao.support.cache.RemoteCache.objectMapper;

/**
 * @Description
 * @Author Vincent.jiao
 * @Date 2022/5/20 13:19
 */
@RequestMapping("demo")
@Controller
public class DemoController {
    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    DataSource dataSource;

    @RequestMapping("demo1")
    @ResponseBody
    public Object demo1() {
        DefaultRedisScript<List> script = new DefaultRedisScript<List>();
        script.setResultType(List.class);
        script.setScriptText(LuaScriptText.getBitRangeScriptText());

        Object data =  redisTemplate.execute(script,
                Arrays.asList("cache:sys:table:person:matedata:bitmap", "200", "500"));
        return data;
    }

    @RequestMapping("demo2")
    @ResponseBody
    public Object demo2() {
        String scriptText = LuaScriptText.getBitRangeScriptText();
        List result = redisTemplate.executePipelined(new RedisCallback<Object>(){
            @Override
            public Object doInRedis(RedisConnection connection) throws DataAccessException {
                connection.scriptLoad(scriptText.getBytes());
                return null;
            }
        }, redisTemplate.getValueSerializer());

        byte[] bitRangeScriptSha = result.get(0).toString().getBytes();

        Object data =  redisTemplate.execute(new RedisCallback() {
            @Override
            public Object doInRedis(RedisConnection connection) throws DataAccessException {
                Object obj = null;
                try {
                    obj = connection.scriptingCommands().evalSha(bitRangeScriptSha,
                            ReturnType.MULTI, 1,
                            objectMapper.writeValueAsString(Arrays.asList("cache:sys:table:person:matedata:bitmap", 200, 500)).getBytes());
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }

                return obj;
            }
        });

        return data;
    }

    @SneakyThrows
    @RequestMapping("demo3")
    @ResponseBody
    public Object demo3() {
        Connection conn = dataSource.getConnection();
        String tableName = "demo"+ RandomUtil.randomInt(10000);
        PreparedStatement ps = conn.prepareStatement("create table "+tableName+ " ( d_id int )");

        return ps.execute();
    }
}
























