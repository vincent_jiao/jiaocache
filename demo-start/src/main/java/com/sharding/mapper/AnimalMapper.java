package com.sharding.mapper;

import com.sharding.entity.Animal;

public interface AnimalMapper {
    int deleteByPrimaryKey(Integer aId);

    int insert(Animal record);

    int insertSelective(Animal record);

    Animal selectByPrimaryKey(Integer aId);

    int updateByPrimaryKeySelective(Animal record);

    int updateByPrimaryKey(Animal record);
}