package com.sharding.mapper;

import com.sharding.entity.Person;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface PersonMapper {
    int deleteByPrimaryKey(Long pId);

    @Delete(" delete from Person where p_sex = #{sex} ")
    int deleteBySex(int sex);

    int insert(Person record);

    int insertBatch(List<Person> record);

    int insertSelective(Person record);

    Person selectByPrimaryKey(Long pId);

    @Select(" select * from person ")
    List<Person> selectAll();

    int updateByPrimaryKeySelective(Person record);

    int updateByPrimaryKey(Person record);
}