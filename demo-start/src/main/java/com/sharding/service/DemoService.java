package com.sharding.service;

import org.springframework.aop.framework.AopContext;
import org.springframework.stereotype.Service;

/**
 * @Author: vincent.jiao
 * @Date: 2021/6/15
 */
@Service
public class DemoService {
    public void testProxy(){
        System.out.println(AopContext.currentProxy());
    }
}
