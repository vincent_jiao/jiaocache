package com.sharding.service;

import com.sharding.entity.Person;
import com.sharding.mapper.PersonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description
 * @Author Vincent.jiao
 * @Date 2022/5/19 14:17
 */
@Service
public class PersonService {
    @Autowired
    PersonMapper personMapper;

    public int deleteByPrimaryKey(Long pId) {
        return personMapper.deleteByPrimaryKey(pId);
    }

    public int deleteBySex(int sex){
        return personMapper.deleteBySex(sex);
    }

    public int insert(Person record){
        return personMapper.insert(record);
    }

    public int insertBatch(List<Person> record){
        return personMapper.insertBatch(record);
    }

    public int insertSelective(Person record){
        return personMapper.insertSelective(record);
    }

    public Person selectByPrimaryKey(Long pId){
        return personMapper.selectByPrimaryKey(pId);
    }

    public int updateByPrimaryKeySelective(Person record){
        return personMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKey(Person record){
        return personMapper.updateByPrimaryKey(record);
    }

    public List<Person> getAll() {
        return personMapper.selectAll();
    }
}
