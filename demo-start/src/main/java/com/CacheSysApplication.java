package com;

import com.jiao.table.lua.IncludeSyntaxEnhance;
import com.jiao.table.lua.LuaScriptLoad;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Author: vincent.jiao
 * @Date: 2021/5/23
 */
@SpringBootApplication
@ComponentScan({"com.sharding", "com.jiao"})
@MapperScan("com.sharding.mapper")
public class CacheSysApplication {
    public static ConfigurableApplicationContext ac;

    public static void main(String[] arsg){
//        IncludeSyntaxEnhance includeSyntaxEnhance = new IncludeSyntaxEnhance();
//        LuaScriptLoad.getInstance().syntaxEnhance();

        ac = SpringApplication.run(CacheSysApplication.class, arsg);
    }
}
