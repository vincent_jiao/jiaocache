package com.jiao.table.lua;

/**
 * lua 语法增强.
 * 由于 redis 内置的 lua 环境做了一些限制, 所以这里需要去增强一些语法
 * @Author: vincent.jiao
 * @Date: 2021/5/31
 */
public interface LuaSyntaxEnhance {
    /**
     * 增强语法
     * @param luaScript lua 脚本内容
     * @return  返回增强的脚本
     */
    public String syntaxEnhance(String luaScript);
}
