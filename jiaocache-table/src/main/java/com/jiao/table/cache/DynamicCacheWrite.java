package com.jiao.table.cache;

import cn.hutool.core.collection.CollectionUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.jiao.comm.exception.CacheSysException;
import com.jiao.datasource.parse.SQLStruct;
import com.jiao.support.cache.DynamicCache;
import com.jiao.table.config.CacheDataLoad;
import com.jiao.table.entity.CacheCommand;
import com.jiao.table.entity.TableInfo;
import com.jiao.table.entity.UpdCacheCommand;
import com.jiao.table.exception.WriteCacheInfoException;
import com.jiao.table.lua.LuaScriptLoad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.ReturnType;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static com.jiao.support.cache.RemoteCache.objectMapper;

/**
 * 动态缓存写入器.
 * @Author: vincent.jiao
 * @Date: 2021/5/9
 */
@Component
public class DynamicCacheWrite implements CacheWrite {
    //TODO 测试完改为私有
    public   CacheWrite remoteWrite = null;
    private CacheWrite localWrite = null;

    @Autowired
    DynamicCacheRead dynamicCacheRead;

    DynamicCache dynamicCache;

    public DynamicCacheWrite(DynamicCache dynamicCache) throws IOException {
        this.dynamicCache = dynamicCache;

        this.remoteWrite = new RemoteWrite();
        this.localWrite = new LocalWrite();
    }

    private CacheWrite getCacheWrite(){
        return dynamicCache.isRemoteCache() ? remoteWrite : localWrite;
    }


    @Override
    public <T> T delete(String tableName, Long id) {
        return getCacheWrite().delete(tableName, id);
    }

    @Override
    public <T> List<T> deleteBatch(String tableName, List<Long> ids) {
        return getCacheWrite().deleteBatch(tableName, ids);
    }

    @Override
    public <T> T update(Object data) {
        return getCacheWrite().update(getCacheCommand(data));
    }

    @Override
    public <T> List<T> updateBatch(Collection datas) {
        return getCacheWrite().updateBatch(datas);
    }

    @Override
    public <T> T add(Object data) {
        return getCacheWrite().add(data);
    }

    @Override
    public <T> List<T> addBatch(Collection datas) {
        return getCacheWrite().addBatch(datas);
    }

    private CacheCommand getCacheCommand(Object data) {
        return CacheCommand.newCacheCommand(data);
    }

    private Collection<CacheCommand> getCacheCommand(Collection datas){
        List<CacheCommand> commands = new LinkedList<>();
        for (Object item : datas) {
            commands.add(getCacheCommand(item));
        }

        return commands;
    }

    private void throwWriteCacheErr(Object entity, String execType) {
        throw new WriteCacheInfoException(entity, execType);
    }

    /**
     * 使用远程缓存.
     * 内部批量操作使用管道 + lua操作 提高性能与并发同时保证数据安全.
     * lua 脚本预先校验存储，避免脚本太长带来的网络开销.
     * 在初始化预加载一次， 避免缓冲区过大 number_of_cached_scripts
     */
    //TODO 测试完改为私有
    public class RemoteWrite implements CacheWrite {
        //TODO 测试完改为私有
        public byte[] addScriptShaStream = null;
        public byte[] delScriptShaStream = null;
        public byte[] updScriptShaStream = null;

        private String addScriptSha = null;
        private String delScriptSha = null;
        private String updScriptSha = null;

        public RemoteWrite() throws IOException {
            Map<String, String> luaScriptMap = LuaScriptLoad.getInstance().getFileAndContentMap();

            if(addScriptShaStream == null){
                List result = dynamicCache.extendCommand((Object... args) -> {
                    RedisConnection connection = (RedisConnection) args[0];
                    connection.scriptLoad(luaScriptMap.get("addCacheData.lua").getBytes());
                    return null;
                });

                addScriptSha = result.get(0).toString();
                addScriptShaStream = addScriptSha.getBytes();
            }

            if(delScriptShaStream == null){
                List result = dynamicCache.extendCommand((Object... args) -> {
                    RedisConnection connection = (RedisConnection) args[0];
                    connection.scriptLoad(luaScriptMap.get("delCacheData.lua").getBytes());
                    return null;
                });

                delScriptSha = result.get(0).toString();
                delScriptShaStream = delScriptSha.getBytes();
            }

            if(updScriptShaStream == null){
                List result = dynamicCache.extendCommand((Object... args) -> {
                    RedisConnection connection = (RedisConnection) args[0];
                    connection.scriptLoad(luaScriptMap.get("updCacheData.lua").getBytes());
                    return null;
                });

                updScriptSha = result.get(0).toString();
                updScriptShaStream = updScriptSha.getBytes();
            }
        }


        @Override
        public <T> T delete(String tableName, Long id) {
            List result = deleteBatch(tableName, Arrays.asList(id));
            if(result == null || result.size() == 0){
                return null;
            }

            return (T) result.get(0);
        }

        @Override
        public <T> List<T> deleteBatch(String tableName, List<Long> ids) {
            if(CollectionUtil.isEmpty(ids)) {
                return Collections.emptyList();
            }

            List datas = dynamicCacheRead.getBatchByPrimaryKey(tableName, ids);
            Collection<CacheCommand> commands = getCacheCommand(datas);

            List result = dynamicCache.extendCommand((Object... args) -> {
                RedisConnection connection = (RedisConnection) args[0];
                for (CacheCommand command : commands) {
                    try {
                        delCommand(connection, command);
                    } catch (JsonProcessingException e) {
                        throw new CacheSysException(e.getMessage(), e);
                    }
                }

                return null;
            });

            isError(result, SQLStruct.DELETE);

            return result;
        }

        @Override
        public <T> T update(Object data) {
            List result = updateBatch(Arrays.asList(data));

            if(result == null || result.size() == 0){
                return null;
            }

            return (T) result.get(0);
        }

        @Override
        public <T> List<T> updateBatch(Collection datas) {
            Collection<CacheCommand> commands = getCacheCommand(datas);

            List result = dynamicCache.extendCommand((Object... args) -> {
                RedisConnection connection = (RedisConnection) args[0];
                for(CacheCommand item : commands){
                    UpdCacheCommand updCacheCommand = getUpdCacheCommand(item);
                    try {
                        updCommand(connection, updCacheCommand);
                    } catch (JsonProcessingException e) {
                        throw new CacheSysException(e.getMessage(), e);
                    }
                }

                return null;
            });

            isError(result, SQLStruct.UPDATE);

            return result;
        }

        private UpdCacheCommand getUpdCacheCommand(CacheCommand cacheCommand){
            UpdCacheCommand command = new UpdCacheCommand();
            command.setAddSha(addScriptSha);
            command.setDelSha(delScriptSha);
            command.setNewCacheCommand(cacheCommand);

            Object value = dynamicCacheRead.getByPrimaryKey(cacheCommand.getTableName(),
                    cacheCommand.getPrimaryKeyValue(), cacheCommand.getData().getClass());

            if(value != null){
                TableInfo tableInfo = CacheDataLoad.getTableInfo(value.getClass());
                command.setOldCacheCommand(CacheCommand.newCacheCommand(value));
            }

            return command;
        }

        @Override
        public <T> T add(Object data) {
            List result = addBatch(Arrays.asList(data));
            if(result == null || result.size() == 0){
                return null;
            }

            return (T) result.get(0);
        }

        @Override
        public <T> List<T> addBatch(Collection datas) {
            Collection<CacheCommand> commands = getCacheCommand(datas);

            List result = dynamicCache.extendCommand((Object... args) -> {
                RedisConnection connection = (RedisConnection) args[0];
                for (CacheCommand command : commands) {
                    try {
                        addCommand(connection, command);
                    } catch (JsonProcessingException e) {
                        throw new CacheSysException(e.getMessage(), e);
                    }
                }

                return null;
            });

            isError(result, SQLStruct.INSERT);
            return result;
        }

        /**
         * 根据命令信息添加缓存.
         * @param connection
         * @param command
         * @return
         */
        public void addCommand(RedisConnection connection, CacheCommand command) throws JsonProcessingException {
            connection.scriptingCommands().evalSha(addScriptShaStream,
                    ReturnType.INTEGER, 1, objectMapper.writeValueAsString(command).getBytes());
        }

        /**
         * 根据命令信息删除缓存.
         * @param connection
         * @param command
         * @return
         */
        public void delCommand(RedisConnection connection, CacheCommand command) throws JsonProcessingException {
            connection.scriptingCommands().evalSha(delScriptShaStream,
                    ReturnType.INTEGER, 1, objectMapper.writeValueAsString(command).getBytes());
        }

        /**
         * 修改数据.
         * 使用 管道 + 事务 修改,保证原子与性能。redis lua 中不允许调用别的脚本, 所以不能用lua。
         * 删除旧数据与新增数据
         * @param connection
         * @param command
         * @throws JsonProcessingException
         */
        public void updCommand(RedisConnection connection, UpdCacheCommand command) throws JsonProcessingException {
            connection.scriptingCommands().evalSha(updScriptShaStream,
                    ReturnType.INTEGER, 1, objectMapper.writeValueAsString(command).getBytes());
        }

        private void isError(List result, String execType) {
            boolean isAdd = SQLStruct.INSERT.equals(execType);

            for (Object item : result) {
                if(item instanceof Long || item instanceof Integer) {
                    int state = Long.valueOf(item+"").intValue();
                    if(state != 0 && !(isAdd && state == 2)) {
                        throwWriteCacheErr(item, SQLStruct.INSERT);
                    }
                }
            }
        }
    }

    /**
     * TODO （内存待完成, 需要首先完成内存缓存，完成版本号）不能用读写锁，用普通锁
     * 使用本地方式操作缓存.
     * 本地需要同样需要保证原子性, 这里使用读写锁做到读写互斥
     */
    private class LocalWrite implements CacheWrite {
        /** 使用读写锁保证数据安全。内部使用公平队列解决读写竞争后先后问题 */
        private ReadWriteLock readWriteLock = new ReentrantReadWriteLock(true);
        private ReentrantReadWriteLock.ReadLock readLock = (ReentrantReadWriteLock.ReadLock) readWriteLock.readLock();
        private ReentrantReadWriteLock.WriteLock writeLock = (ReentrantReadWriteLock.WriteLock) readWriteLock.writeLock();

        @Override
        public <T> T delete(String tableName, Long id) {
            return null;
        }

        @Override
        public <T> List<T> deleteBatch(String tableName, List<Long> ids) {
            return null;
        }

        @Override
        public <T> T update(Object data) {
            return null;
        }

        @Override
        public <T> List<T> updateBatch(Collection datas) {
            return null;
        }

        @Override
        public <T> T add(Object data) {
            return null;
        }

        @Override
        public <T> List<T> addBatch(Collection datas) {
            return null;
        }
    }
}
