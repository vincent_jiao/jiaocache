package com.jiao.table.cache;

import com.jiao.comm.utils.LogUtil;
import com.jiao.comm.utils.SpringUtils;
import com.jiao.datasource.parse.SQLStruct;
import com.jiao.table.config.CacheDataLoad;
import com.jiao.table.entity.Compensate;
import com.jiao.table.entity.TableInfo;
import com.jiao.table.jdbc.TableMapperService;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Timer;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * 缓存补偿.
 * 如果缓存出错了，那么将尝试重新加入
 * @Author Vincent.jiao
 * @Date 2022/5/23 11:36
 */
public class CacheCompensate {
    //尝试时间建割
    public static final int[] attemptTimeInterval= {5, 30, 60};

    private static DynamicCacheWrite dynamicCacheWrite = null;

    static {
        new Thread(CacheCompensate::remedy).start();
    }

    private static final TreeMap<Long, Compensate> compensateMap = new TreeMap<>(new Comparator<Long>(){
        @Override
        public int compare(Long o1, Long o2) {
            return o1.compareTo(o2);
        }
    });

    public static synchronized void register(Compensate compensate) {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        for (int i = 2; i < stackTrace.length; i++) {
            StackTraceElement element = stackTrace[i];
            //重试过程中出现那就不加入
            if(CacheCompensate.class.getName().equals(element.getClassName())) {
                return;
            }
        }

        long currTime = System.currentTimeMillis();
        currTime += attemptTimeInterval[compensate.getCompensateCount()] * 1000L;
        compensateMap.put(currTime, compensate);
    }

    private static void remedy(){
        while (true){
            Map.Entry<Long, Compensate> entry = null;
            synchronized (CacheCompensate.class){
                entry =  compensateMap.firstEntry();
                //如果未到时间不执行
                if(entry == null || entry.getKey() > System.currentTimeMillis()) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        LogUtil.error(e.getMessage(), e);
                    }
                    continue;
                }

                compensateMap.remove(entry.getKey());
            }

            Compensate compensate = entry.getValue();
            Long idVal = null;
            Class entityClass = compensate.getEntity().getClass();
            try {
                TableInfo tableInfo = CacheDataLoad.getTableInfo(entityClass);
                String tableName = tableInfo.getTableName();
                Field field = entityClass.getDeclaredField(tableInfo.getPrimaryKeyName());
                field.setAccessible(true);
                idVal = field.getLong(compensate.getEntity());
                Object newEntity = TableMapperService.getInstance().selectByPrimaryKey(tableName, idVal, CacheDataLoad.getTableEntityClass(tableName));

                if(newEntity == null) {
                    //数据已不存在, 取消补偿
                    LogUtil.info("数据已不存在, 取消补偿。信息:" + entityClass.getName()
                            + ", 操作：" + compensate.getSqlType() + " id = " + idVal);
                    continue;
                }

                if(SQLStruct.INSERT.equals(compensate.getSqlType())) {
                    getDynamicCacheWrite().add(newEntity);
                } else if(SQLStruct.UPDATE.equals(compensate.getSqlType())) {
                    getDynamicCacheWrite().update(newEntity);
                } else if(SQLStruct.DELETE.equals(compensate.getSqlType())) {
                    getDynamicCacheWrite().delete(tableInfo.getTableName(), Long.valueOf(idVal+""));
                }

                LogUtil.info("数据修复成功, 信息:" + entityClass.getName()
                        + ", 操作：" + compensate.getSqlType() + " id = " + idVal);

            } catch (Exception e) {
                //再入队
                int count = compensate.getCompensateCount() + 1;
                if(count > attemptTimeInterval.length) {
                    LogUtil.error("数据无法修复, 最终该错误原因:" + e.getMessage() + ", compensate 信息:" + compensate, e);
                    continue;
                }

                LogUtil.error("数据修复失败, 信息:" + entityClass.getName()
                        + ", 操作：" + compensate.getSqlType() + " id = " + idVal+". errorMsg:" + e.getMessage(), e);

                compensate.setCompensateCount(count);
                compensate.addErrorMsg(e.getMessage());
                register(compensate);
            }
        }
    }


    public static DynamicCacheWrite getDynamicCacheWrite(){
        if(dynamicCacheWrite == null) {
            dynamicCacheWrite = SpringUtils.getBean("dynamicCacheWrite");
        }
        return dynamicCacheWrite;
    }
}
