package com.jiao.table.cache;

import com.jiao.support.cache.DynamicCache;
import com.jiao.support.rule.ExecBatch;
import com.jiao.syntax.impl.Expression;
import com.jiao.table.config.CacheDataLoad;
import com.jiao.table.config.CacheKeyDefinition;
import com.jiao.table.csql.CacheResultMapping;
import com.jiao.table.csql.CalculationCselExpression;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * 动态缓存读取.
 * @Author: vincent.jiao
 * @Date: 2021/5/13
 */
@Component
public class DynamicCacheRead {
    @Autowired
    DynamicCache dynamicCache;

    /**
     * 提供快捷快速查询.
     * @param tableName
     * @param id
     * @param <T>
     * @return when used in pipeline / transaction.
     */
    public <T> T getByPrimaryKey(String tableName, Long id) {
        Class clazz = CacheDataLoad.getTableEntityClass(tableName.toLowerCase(Locale.ROOT));
        return getByPrimaryKey(tableName, id, clazz);
    }

    /**
     * 提供快捷快速查询.
     * @param tableName
     * @param id
     * @param <T>
     * @return when used in pipeline / transaction.
     */
    public <T> T getByPrimaryKey(String tableName, Long id, Class dataClass){
        String key = CacheKeyDefinition.getTableDataKey(tableName.toLowerCase(Locale.ROOT));
        String value = dynamicCache.mapGet(key, Long.toString(id));

        return CacheResultMapping.toBean(tableName, value, dataClass);
    }

    /**
     * 提供快捷快速批量查询.
     * @param tableName
     * @param id
     * @param <T>
     * @return when used in pipeline / transaction.
     */
    public <T> List<T> getBatchByPrimaryKey(String tableName, Collection<Long> ids){
        Class clazz = CacheDataLoad.getTableEntityClass(tableName.toLowerCase(Locale.ROOT));
        return getBatchByPrimaryKey(tableName, ids, clazz);
    }

    public <T> List<T> getBatchByPrimaryKey(String tableName, Collection<Long> ids, Class dataClass){
        String key = CacheKeyDefinition.getTableDataKey(tableName.toLowerCase(Locale.ROOT));

        List<String> datas = dynamicCache.extendCommand(new ExecBatch<String>() {
            @Override
            public String exec(Object... args) {
                RedisConnection connection = (RedisConnection) args[0];
                RedisTemplate redisTemplate = (RedisTemplate) args[1];
                RedisSerializer redisSerializer = redisTemplate.getHashKeySerializer();

                for (Long item : ids) {
                    connection.hGet(redisSerializer.serialize(key),
                            redisSerializer.serialize(Long.toString(item)));
                }

                return null;
            }
        });

        List<T> resultList = new LinkedList<>();
        for (String item : datas) {
            resultList.add(CacheResultMapping.toBean(tableName, item, dataClass));
        }

        return resultList;
    }

    @SneakyThrows
    public <T> T getValueByCSQL(String csql, Object... params) {
        return getValueByCSQL(csql, null, params);
    }

    @SneakyThrows
    public <T> T getValueByCSQL(String csql, Map<String, Object> params) {
        return getValueByCSQL(csql, null, params);
    }

    public <T> T getValueByCSQL(String csql, Class beanClass, Object... params) {
        Map<String, Object> paramsMap = new HashMap<>();
        if(params != null && params.length > 0) {
            for (int i = 0; i < params.length; i++) {
                paramsMap.put(String.valueOf(i), params[i]);
            }
        }

        return getValueByCSQL(csql, beanClass, paramsMap);
    }

    @SneakyThrows
    public <T> T getValueByCSQL(String csql, Class beanClass, Map<String, Object> params) {
        Expression expression = new CalculationCselExpression(csql);
        if(beanClass == null) {
            beanClass = CacheDataLoad.getTableEntityClass(expression.getExpressionTableName());
        }

        return CacheResultMapping.toBean(expression.getValue(), beanClass);
    }


















    public <T> List<T> getValueListByCSQL(String csql, Object... params) {
        return getValueListByCSQL(csql, null, params);
    }

    public <T> List<T> getValueListByCSQL(String csql, Map<String, Object> params) {
        return getValueListByCSQL(csql, null, params);
    }

    public <T> List<T> getValueListByCSQL(String csql, Class beanClass, Object... params) {
        Map<String, Object> paramsMap = new HashMap<>();
        if(params != null && params.length > 0) {
            for (int i = 0; i < params.length; i++) {
                paramsMap.put(String.valueOf(i), params[i]);
            }
        }

        return getValueListByCSQL(csql, beanClass, paramsMap);
    }

    @SneakyThrows
    public <T> List<T> getValueListByCSQL(String csql, Class beanClass, Map<String, Object> params) {
        Expression expression = new CalculationCselExpression(csql);
        Class thisClass =  CacheDataLoad.getTableEntityClass(expression.getExpressionTableName());
        beanClass = beanClass != null ? beanClass : thisClass;

        List valueList = expression.getValueList(params);
        if(thisClass.equals(beanClass)) {
            return valueList;
        }

        List<T> result = new LinkedList<>();
        for (Object item : valueList) {
            result.add(CacheResultMapping.toBean(item, beanClass));
        }

        return result;
    }
}
