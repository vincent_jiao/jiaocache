package com.jiao.table.cache;

import java.util.Collection;
import java.util.List;

/**
 * 缓存写入器.
 * @Author: vincent.jiao
 * @Date: 2021/5/9
 */
public interface CacheWrite {
    /**
     * 删除数据.
     * @param tableName
     * @param id
     * @return 0: 执行成功   1:执行失败  2:未知错误
     */
    public <T> T delete(String tableName, Long id);

    /**
     * 集合删除数据.
     * @param tableName
     * @param ids
     * @return 0: 执行成功   1:执行失败  2:未知错误
     */
    public <T> List<T> deleteBatch(String tableName, List<Long> ids);

    /**
     * 修改数据.
     * @param data
     * @return --0: 执行成功   1:删除失败  2:新增失败   3:未知错误
     */
    public <T> T update(Object data);

    /**
     * 集合修改数据
     * @param datas
     * @return --0: 执行成功   1:删除失败  2:新增失败   3:未知错误
     */
    public <T> List<T> updateBatch(Collection datas);

    /**
     * 新增数据.
     * @param data
     * @return 0: 执行成功   1:执行失败  2:数据已存在  3:回滚失败 4:执行失败但是回滚成功
     */
    public <T> T add(Object data);

    /**
     * 新增批量数据.
     * @param datas
     * @return 0: 执行成功   1:执行失败  2:数据已存在  3:回滚失败 4:执行失败但是回滚成功
     */
    public <T> List<T> addBatch(Collection datas);
}
