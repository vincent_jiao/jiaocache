package com.jiao.table.cache;

import com.jiao.comm.exception.DataIncompleteException;
import com.jiao.comm.utils.CollectionNullUtils;
import com.jiao.comm.utils.TypeConvert;
import com.jiao.support.cache.DynamicCache;
import com.jiao.support.rule.ExecBatch;
import com.jiao.table.config.CacheDataLoad;
import com.jiao.table.config.CacheKeyDefinition;
import com.jiao.table.entity.TableInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * @Description
 * @Author Vincent.jiao
 * @Date 2022/5/19 16:48
 */
@Component
public class DynamicReadMateData {
    @Autowired
    DynamicCache dynamicCache;

    /**
     * 获取元数据中索引的全部值.
     * @param indexName
     * @return
     */
    public <T> Set<T> getMateDateIndexValueSet(String tableName, String indexName){
        TableInfo tableInfo = CacheDataLoad.getTableInfo(tableName);
        if(tableInfo == null) {
            return Collections.emptySet();
        }

        Set<T> result = dynamicCache.setgetAll(tableInfo.getMateDataIndexAllValueKey(indexName));
        return CollectionNullUtils.getSet(result);
    }

    /**
     * 获取元数据中指定索引 key 的全部值.
     * @param indexkey
     * @param indexvalue
     * @return
     */
    public <T> Set<T> getMateDateIndexValueSet(String tableName, String indexkey, String indexvalue){
        TableInfo tableInfo = CacheDataLoad.getTableInfo(tableName);
        if(tableInfo == null) {
            return Collections.emptySet();
        }

        Set<T> result = dynamicCache.setgetAll(tableInfo.getIndexValueKey(indexkey, indexvalue));
        return CollectionNullUtils.getSet(result);
    }

    /**
     * 批量根据索引名与值获取到表的主键集合.
     * @param indexName
     * @param indexValue
     * @return
     */
    public <T> Set<T> getPKSetByIndexValueBatch(String tableName, String indexName, Set indexValue){
        if(indexName == null){
            return Collections.emptySet();
        }

        TableInfo tableInfo = CacheDataLoad.getTableInfo(tableName);
        if(tableInfo == null) {
            return Collections.emptySet();
        }

        Set<T> result = null;

        //批量查询
        if(dynamicCache.isRemoteCache()){
            //管道查询
            List<Set<T>> batchResult = dynamicCache.extendCommand(new ExecBatch<Set<T>>() {
                @Override
                public Set<T> exec(Object... args) {
                    RedisConnection connection = (RedisConnection) args[0];

                    for (Object item : indexValue){
                        String indexKey = tableInfo.getIndexValueKey(indexName, item.toString());
                        connection.sMembers(indexKey.getBytes());
                    }
                    return null;
                }
            });

            //合并结果
            if(batchResult != null){
                if(batchResult.isEmpty()){
                    result = Collections.emptySet();
                } else {
                    result = new HashSet<>();
                    for (Set<T> item : batchResult){
                        result.addAll(item);
                    }
                }
            }

        } else {
            String indexKey = null;
            result = new HashSet<>();
            for (Object item : indexValue){
                indexKey = tableInfo.getIndexValueKey(indexKey, item.toString());
                Set<T> tmpSet = dynamicCache.setgetAll(indexKey);

                if(tmpSet != null && !tmpSet.isEmpty()){
                    result.addAll(tmpSet);
                }
            }
        }

        return result;
    }

    /**
     * 获取指定元数据索引中, 指定范围的集合
     * @param indexName
     * @param min   下限
     * @param max   上限,当大于0时候表示无上限
     * @return
     */
    public Set<Long> getRangeSetByMateDataIndexKey(String tableName, String indexName, long min, long max){
        TreeSet<Long> treeSet = new TreeSet<>(getMateDateIndexValueSet(tableName, indexName));

        //获取指定范围，+1 表示包含上限
        Set<Long> resultSet = null;
        max = max > 0 ? max : treeSet.last();
        treeSet.subSet(min, max  + 1);

        return convertSetLong(resultSet);
    }

    /**
     * 根据索引的范围，返回对应筛选的全部主键数据（仅支持数值类型索引）.
     * @param indexName
     * @param min
     * @param max
     * @return
     */
    public Set<Long> getPKSetByRangeSet(String tableName, String indexName, long min, long max){
        //获取此字段对应的所有值
        Set<Long> indexValueSet = getRangeSetByMateDataIndexKey(tableName, indexName, min, max);

        if(CollectionNullUtils.isEmpty(indexValueSet)){
            return Collections.emptySet();
        }


        TableInfo tableInfo = CacheDataLoad.getTableInfo(tableName);
        return convertSetLong(getPKSetByIndexValueBatch(tableName, tableInfo.getIndexKey(indexName)
                , indexValueSet));
    }


    /**
     * 获取 bitmap 字典中范围内存在的值.
     * @param min
     * @param max
     * @return
     */
    public Set<Long> getBitGetExistsBit(String tableName,long min, long max){
        TableInfo tableInfo = CacheDataLoad.getTableInfo(tableName);
        if(tableInfo == null) {
            return Collections.emptySet();
        }

        String key = tableInfo.getMateDataBitMapKey();
//        List<String> rangeList = new LinkedList<>();
//        LongStream.range(min, max).forEach(i -> rangeList.add(String.valueOf(i)));

        Collection<Long> strIds = dynamicCache.bitGetExistsBit(key, min, max);
        if(CollectionNullUtils.isEmpty(strIds)){
            return Collections.emptySet();
        }

        return convertSetLong(new HashSet<>(strIds));
    }

    /**
     * 获取 bitmap 字典中范围内存在的值.
     * @return
     */
    public Set<Long> getBitGetExistsBit(String tableName, Collection ids){
        TableInfo tableInfo = CacheDataLoad.getTableInfo(tableName);
        if(tableInfo == null) {
            return Collections.emptySet();
        }

        String key = tableInfo.getMateDataBitMapKey();
        ids = TypeConvert.collectionToLong(ids);
        Collection<Long> strIds = dynamicCache.bitGetExistsBit(key, ids);
        if(CollectionNullUtils.isEmpty(strIds)){
            return Collections.emptySet();
        }

        return convertSetLong(new HashSet<>(strIds));
    }

    /**
     * 根据索引名和值，获取所有主键的映射.
     * @param indexName
     * @param indexValue
     * @param <T>
     * @return
     */
    public  <T> Set<Long> getPKSetByIndexValue(String tableName, String indexName, T indexValue){
        TableInfo tableInfo = CacheDataLoad.getTableInfo(tableName);
        Set<Long> resultSet = dynamicCache.setgetAll(tableInfo.getIndexValueKey(indexName, indexValue.toString()));

        return convertSetLong(CollectionNullUtils.getSet(resultSet));
    }

    public Long getMaxIdNum(String tableName) {
        TableInfo tableInfo = CacheDataLoad.getTableInfo(tableName);
        Object obj = dynamicCache.valueGet(tableInfo.getMateDataMaxIdNumKey());
        return TypeConvert.objectToLong(obj);
    }

    /**
     * 获取元数据中最后修改时间.
     * @param key
     * @return
     */
    public String getMateDataVersion(String key){
        String version = dynamicCache.valueGet(key);
        if(version == null) {
            throw new DataIncompleteException("未找到版本号索引不完整");
        }

        return version;
    }

    /**
     * 获取全部id.
     */
    public Set<Long> getAllId(String tableName) {
        TableInfo tableInfo = CacheDataLoad.getTableInfo(tableName);
        Set allId = dynamicCache.mapKeys(CacheKeyDefinition.getTableDataKey(tableName));
        return convertSetLong(allId);
    }

    private Set<Long> convertSetLong(Set ids){
        if (CollectionNullUtils.isEmpty(ids)){
            return Collections.emptySet();
        }

        Set<Long> newValue = new HashSet<>(ids.size(), 1.0f);
        for (Object id : ids) {
            if (id.getClass().equals(Long.class)){
                newValue.add((Long)id);
            } else if (id.getClass().equals(String.class)){
                newValue.add(Long.valueOf(id.toString()));
            }
        }

        return newValue;
    }
}
