package com.jiao.table.listener;

import com.jiao.datasource.listener.ConnectionListener;
import com.jiao.datasource.listener.SQLListener;
import com.jiao.datasource.parse.SQLCommand;

import static com.jiao.datasource.parse.SQLStruct.*;
import static com.jiao.datasource.parse.SQLStruct.INSERT;

/**
 * @Description
 * @Author Vincent.jiao
 * @Date 2022/5/17 13:49
 */
public abstract class SQLExecListenerAbstract implements SQLListener, ConnectionListener {
    public boolean isDelete(SQLCommand sqlCommand) {
        return DELETE.equals(sqlCommand.getCommandType());
    }

    public boolean isSelect(SQLCommand sqlCommand) {
        return SELECT.equals(sqlCommand.getCommandType());
    }

    public boolean isUpdate(SQLCommand sqlCommand) {
        return UPDATE.equals(sqlCommand.getCommandType());
    }

    public boolean isInsert(SQLCommand sqlCommand) {
        return INSERT.equals(sqlCommand.getCommandType());
    }
}
