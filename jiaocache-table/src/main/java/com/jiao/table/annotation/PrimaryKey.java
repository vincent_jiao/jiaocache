package com.jiao.table.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author: vincent.jiao
 * @Date: 2021/4/28
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PrimaryKey {
    //如果存在序列就写不存在则不写
    public String seqName() default  "";

    //是否手动生成
    public boolean isManualGeneration() default false;
}
