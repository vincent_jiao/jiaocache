package com.jiao.table.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 结果列映射.
 * @Author: vincent.jiao
 * @Date: 2021/6/6
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ResultColunmMapper {
    /** 列名 */
    public String value();
}
