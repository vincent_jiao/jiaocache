package com.jiao.table.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 列映射.
 * 注解到实体上，假如实体列名与数据库不一致，则用这个去映射指定数据库列名.
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ColunmMapping {
    public String name();
}
