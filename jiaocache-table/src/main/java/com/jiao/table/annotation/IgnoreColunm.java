package com.jiao.table.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 忽略列.
 * 当被 @TableCache 注解的实体中，存在数据库中不存在的字段时候，需要用这个忽略.
 * @Author Vincent.jiao
 * @Date 2022/6/8 9:52
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IgnoreColunm {
}
