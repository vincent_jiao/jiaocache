package com.jiao.table.config;

/**
 * 缓存 key 定义.
 * 这里定义缓存存放的结构
 * @Author: vincent.jiao
 * @Date: 2021/4/17
 */
public class CacheKeyDefinition {
    public final static String CACHE_KEY = "cache:sys:table:";

    public static String getTableDataKey(String tableName){
        return CACHE_KEY + tableName + ":data";
    }

    public static String getIndexKey(String tableName, String indexName){
        return CACHE_KEY + tableName + ":index:" + indexName;
    }

    public static String getIndexValueKey(String indexKey, String indexV){
        return indexKey +":"+indexV;
    }

    public static String getIndexValueKey(String tableName, String indexName, String indexV){
        return CACHE_KEY + tableName + ":index:" + indexName +":"+indexV;
    }

    public static String getMateDataMaxIdNumKey(String tableName){
        return CACHE_KEY + tableName + ":matedata:maxIdNum";
    }

    public static String getMateDataBitMap(String tableName){
        return CACHE_KEY + tableName + ":matedata:bitmap";
    }

    /**
     * 获取索引全部值的key
     * @param tableName
     * @param index
     * @return
     */
    public static String getMateDataIndexAllValueKey(String tableName, String index){
        return CACHE_KEY + tableName + ":matedata:index:"+index;
    }

    public static String getMateVersionTime(String tableName){
        return CACHE_KEY + tableName + ":matedata:version";
    }

    /**
     * 返回数据当前状态值（是否可用）.
     * 0 不可用  1 可用
     * @param tableName
     * @return
     */
    public static String getDataStatus(String tableName){
        return CACHE_KEY + tableName + ":matedata:status";
    }
}
