package com.jiao.table.exception;

import com.jiao.comm.exception.CacheSysException;
import com.jiao.table.cache.CacheCompensate;
import com.jiao.table.entity.Compensate;

/**
 * @Description
 * @Author Vincent.jiao
 * @Date 2022/5/23 13:34
 */
public class WriteCacheInfoException extends CacheSysException {
    private Object entity;
    private String execType;

    public WriteCacheInfoException(Object obj, String execType) {
        super("在写 "+execType+" 类型缓存时出现异常");
        Compensate compensate = new Compensate();
        compensate.setEntity(obj);
        compensate.setSqlType(execType);
        CacheCompensate.register(compensate);   //加入补偿队列
    }
}
