package com.jiao.table.entity;

import lombok.Data;

/**
 * @Author: vincent.jiao
 * @Date: 2021/5/16
 */
@Data
public class UpdCacheCommand {
    /** 增加新数据的命令 */
    private CacheCommand newCacheCommand;
    /** 删除旧数据的命令 */
    private CacheCommand oldCacheCommand;

    private String addSha;
    private String delSha;
}
