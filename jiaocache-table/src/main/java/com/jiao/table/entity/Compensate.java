package com.jiao.table.entity;

import cn.hutool.core.util.StrUtil;
import lombok.Data;

import java.util.LinkedList;
import java.util.List;

/**
 * @Description
 * @Author Vincent.jiao
 * @Date 2022/5/23 11:46
 */
@Data
public class Compensate {
    private String sqlType;

    private Object entity;

    private int compensateCount = 0;

    private List<String> errorMsgList;

    public void addErrorMsg(String msg) {
        if(errorMsgList == null) {
            errorMsgList = new LinkedList<>();
        }

        errorMsgList.add(msg);
    }

    @Override
    public String toString() {
        return "sqlType="+sqlType+", errorMsgList="+(StrUtil.join(",", errorMsgList))
                +", entity class="+(entity.getClass().getName());
    }
}
