package com.jiao.table.entity;

import com.jiao.datasource.parse.SQLUnitAbstract;
import lombok.Data;

import java.sql.SQLType;

/**
 * 对于一个 sql 简单的解析信息.
 * @Author : Vincent.jiao
 * @Date : 2021/8/15 10:51
 * @Version : 1.0
 */
@Data
public class SQLSimpleDMLInfo {
    //sql执行类型
    private SQLType sqlExceType;
    //表名
    private String tableName;

    private String sql;

    private SQLUnitAbstract sqlUnit;

    private String whereCondition;

    private TableInfo tableInfo;
}
