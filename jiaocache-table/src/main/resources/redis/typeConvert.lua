local typeConvert = function (value)
    if(type(value) == "nil")
    then
        return "null";
    end;

    if(type(value) == "boolean")
    then
        return tostring(value);
    end;

    if(type(value) == "table")
    then
        return cjson.encode(value);
    end;

    if(type(value) == "userdata")
    then
        return cjson.encode(value);
    end;

    return value;
end;