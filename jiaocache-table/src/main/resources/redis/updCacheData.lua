include("typeConvert.lua");

-- 首先删除旧的，然后再新增
local dataJson = cjson.decode(KEYS[1]);

local addSha = dataJson["addSha"];
local delSha = dataJson["delSha"];
local newCacheCommand = typeConvert(dataJson["newCacheCommand"]);
local oldCacheCommand = typeConvert(dataJson["oldCacheCommand"]);
local resultV = 1;

local updCache = function()
    local status = 0;

    -- 如果存在旧数据就删除
    if(oldCacheCommand ~= 'null')
    then
        include("delCacheCoreScript.lua");
        status = delCache(oldCacheCommand);
    end;

    if(status == 0)
    then
        include("addCacheCoreScript.lua");
        status = addCache(newCacheCommand);

        if(status ~= 0)
        then
            resultV = 2;
        else
            resultV = 0;
        end;
    else
        resultV = 1;
    end;

end;

local errorHandler = function(e)
    redis.log(redis.LOG_NOTICE, e);
    redis.log(redis.LOG_NOTICE, "updErrorInfo >>> script error info: ", cjson.encode(newCacheCommand))
    resultV = 3;
end;

xpcall(updCache, errorHandler);

--0: 执行成功   1:删除失败  2:新增失败   3:未知错误
return resultV;