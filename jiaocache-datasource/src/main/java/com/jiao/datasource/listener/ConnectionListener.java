package com.jiao.datasource.listener;

import com.jiao.comm.exception.CloseLoopException;
import com.jiao.comm.exception.ListenerException;
import com.jiao.datasource.parse.SQLCommand;

import javax.activation.CommandInfo;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface ConnectionListener {
    /**
     * 关闭链接前.
     */
    public void close(Connection conn) throws ListenerException;

    /**
     * 获取连接后事件.
     */
    public void open(Connection conn) throws ListenerException;


    /**
     * 监听 sql 写操作(commit前).
     */
    public void commiteBefore(List<SQLCommand> sqlCommands) throws ListenerException;

    /**
     * 监听 sql 写操作(commit后).
     */
    public void commiteAfter(List<SQLCommand> sqlCommands)  throws ListenerException;
}
