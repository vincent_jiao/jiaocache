package com.jiao.datasource.listener;

import com.jiao.comm.exception.ListenerException;
import com.jiao.datasource.parse.SQLCommand;

import java.util.List;

/**
 * sql 监听器.
 * 当发生 DML 语言操作时候会发生的事件
 * @Author: vincent.jiao
 * @Date: 2021/5/3
 */
public interface SQLListener {
    /**
     * 监听执行 sql 之前操作.
     */
    public void execBefore(SQLCommand commandInfo)  throws ListenerException;

    /**
     * 监听 sql 写之后操作.
     */
    public void execAfter(SQLCommand commandInfo) throws ListenerException;
}
