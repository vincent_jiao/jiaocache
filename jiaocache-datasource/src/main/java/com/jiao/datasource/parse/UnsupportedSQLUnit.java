package com.jiao.datasource.parse;

import java.sql.Connection;
import java.sql.Statement;

/**
 * @Description
 * @Author Vincent.jiao
 * @Date 2022/6/1 9:31
 */
public class UnsupportedSQLUnit extends SQLUnitAbstract{
    public UnsupportedSQLUnit(Connection connection, Statement statement) {
        super(connection, statement);
    }
}
