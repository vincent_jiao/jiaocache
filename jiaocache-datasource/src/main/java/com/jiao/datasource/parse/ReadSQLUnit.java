package com.jiao.datasource.parse;

import java.sql.Connection;
import java.sql.Statement;

/**
 * 写类 sql 执行时候单元信息.
 * @Author: vincent.jiao
 * @Date: 2021/5/3
 */
public class ReadSQLUnit extends SQLUnitAbstract {
    public ReadSQLUnit(Connection connection, Statement statement) {
        super(connection, statement);
    }
}
