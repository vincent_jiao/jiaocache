package com.jiao.datasource.parse;

import lombok.Getter;
import lombok.Setter;

import java.sql.Connection;
import java.sql.Statement;

/**
 * 写类 sql 执行时候单元信息.
 * @Author: vincent.jiao
 * @Date: 2021/5/3
 */
public class WriteSQLUnit extends SQLUnitAbstract {
    @Setter
    @Getter
    /** 受影响的行数 */
    private Integer count;

    public WriteSQLUnit(Connection connection, Statement statement) {
        super(connection, statement);
    }
}
