package com.jiao.datasource.parse;

import lombok.Data;

/**
 * @Description
 * @Author Vincent.jiao
 * @Date 2022/5/16 17:37
 */
@Data
public class SQLStruct {
    public static final String SELECT = "SELECT";
    public static final String UPDATE = "UPDATE";
    public static final String INSERT = "INSERT";
    public static final String DELETE = "DELETE";

    public static final String FROM = "FROM";
    public static final String WHERE = "WHERE";

    private String commandType;
    private String tableName;
    private String whereSql;
    private String colunmSql;
    private String sql;

    public SQLStruct() {}
    public SQLStruct(String sql) {this.sql = sql;}
}
